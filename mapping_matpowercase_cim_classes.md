# Mappings

## Branches
### PiLine (Line mit `ratio = 0`)

- Erstelle Terminal mit `sequenceNumber` = 1 und verbine mit TopologicalNode des FromBus 
- Erstelle Terminal mit `sequenceNumber` = 2 und verbine mit TopologicalNode des ToBus
- Zeige mit beiden Terminals auf ACLineSegment

| Line | CIM-Attribut |
|------|--------------|
| r    | ACLineSegment->r.value = r*(fbus.baseKV)^2/baseMVA |
| x    | ACLineSegment->x.value = x*(fbus.baseKV)^2/baseMVA |
| b    | ACLineSegment->bch.value = b*baseMVA/(fbus.baseKV)^2 |
| N/A  | ACLineSegment->gch.value = 0 |
| N/A  | ACLineSegment->length.value = 1 |
| N/A  | Von zugehöriger `TopologicalNode` (fbus) die `BaseVoltage->nominalVoltage.value` |


### Transformer (Line mit `ratio != 0`)

- Erstelle PowerTransformerEnd mit `endNumber` = 1 ("PowerTransformerEnd_1")
- Erstelle PowerTransformerEnd mit `endNumber` = 2 ("PowerTransformerEnd_2")
- Erstelle Terminal mit `sequenceNumber` = 1 und verbine mit TopologicalNode des FromBus 
- Erstelle Terminal mit `sequenceNumber` = 2 und verbine mit TopologicalNode des ToBus
- Zeige mit beiden Terminals auf PowerTransformer
- Zeige mit PowerTransformer auf beide PowerTransformerEnds
- Zeige in beiden PowerTransformerEnds auf den jeweligen Terminal

| Transformer | CIM-Attribut |
|-------------|--------------|
| r     | PowerTransformerEnd_1->r.value = r*(fbus.baseKV)^2/baseMVA  |
| x     | PowerTransformerEnd_1->x.value = x*(fbus.baseKV)^2/baseMVA  |
| N/A   | PowerTransformerEnd_1->g.value = 0                          |
| b     | PowerTransformerEnd_1->b.value = b*baseMVA/(fbus.baseKV)^2  |
| rateA | PowerTransformerEnd_1->ratedS.value = rateA , PowerTransformerEnd_1->ratedS = rateA |
| FromBus.baseKV | PowerTransformerEnd_1->ratedU.value |
| ToBus.baseKV | PowerTransformerEnd_2->ratedU.value |

- Erstelle RatioTapChanger und verbinde mit PowerTransformerEnd_1

| Transformer | CIM-Attribut |
|-------------|--------------|
| N/A         | PowerTransformerEnd_1->RatioTapChanger.neutralStep = 0 |
| ratio       | PowerTransformerEnd_1->RatioTapChanger.normalStep = (ratio-1)*1000 |
| N/A         | PowerTransformerEnd_1->RatioTapChanger.stepVoltageIncrement = 0.1 |

Falls `angle != 0`

**TBD**

| Transformer | CIM-Attribut |
|-------------|--------------|
| angle       | PowerTransformerEnd_1->phaseAngleClock = static_cast<int>int(angle/30)|


- Notizen:
    - siehe auch PowerTransformer in [CIMverter](https://git.rwth-aachen.de/acs/core/cim/CIMverter/blob/release/src/CIMObjectHandler.cpp#L639)
    - Laut CIM-Standard "soll" PowerTransformerEnd_1 die Hochspannungsseite sein, was hier jedoch (wie auch in NEPLAN) unbeachtet bleibt.

## Busse

**Anmerkung:** 

Die Parametrisierung der Komponenten kann in CIM auf unterschiedliche Arten realisiert werden:
- Mittels des Setzens enstprechender Attribute der jeweiligen Komponente (z.B. `p.value` von `EnergyConsumer`)
- Mittels des Erstellens von entsprechenden `SvPowerFlow`/`SvVoltage`-Objekten und Verbinden mit dem entsprechenden `Terminal`/`TopologicalNode` (z.B. Setzen des `p.value` eines `SvPowerFlow`-Objekts, welches mit dem `Terminal` vom `EnergyConsumer` verbunden ist)

In MPC2CIM und im CIMverter kann jeweils einer der beiden Ansätze zum Setzen/Einlesen der Parametrisierung gewählt werden (Flags: `useSVforEnergyConsumer`, `useSVforGeneratingUnit`, `useSVforExternalNetworkIjection`).

### Typ 1 (PQ Bus)
- TopologicalNode erstellen
- EnergyConsumer erstellen und mit TopologicalNode verbinden
- TopologicalNode mit Terminal verbinden und Terminal mit EnergyConsumer verbinden
- SvPowerFlow erstellen und mit Terminal verbinden

| PQ Bus | CIM-Attribut |
|--------|--------------|
| baseKV | Von zugehöriger `TopologicalNode` die `BaseVoltage->nominalVoltage.value` |
| Pd     | Vom zug. `EnergyConsumer` die `p.value` |
| Qd     | Vom zug. `EnergyConsumer` die `q.value` |
| Pd     | Vom zug. `SvPowerFlow` die `p.value` |
| Qd     | Vom zug. `SvPowerFlow` die `q.value` |

Falls `Gs != 0` oder `Bs != 0`:

- `EquivalentShunt` erstellen und mit TopologicalNode verbinden
- TopologicalNode mit Terminal verbinden und Terminal mit `EquivalentShunt` verbinden

| PQ Bus | CIM-Attribut |
|--------|--------------|
| Gs, baseKV | EquivalentShunt->g=Gs/(baseKV)^2 |
| Bs, baseKV | EquivalentShunt->b=Bs/(baseKV)^2 |


### Typ 2 (PV Bus)
- TopologicalNode erstellen
- mit einem einem daran angeschlossenen Objekt der Klasse `GeneratingUnit`, die mit ihrem Attribut `RotatingMachine` auf eine `SynchronousMachine` zeigt (eine `SynchronousMachine` in der std::list)

Aus der Bus-Information:

| PV Bus | CIM-Attribut |
|--------|--------------|
| baseKV | Von zugehöriger `TopologicalNode` die `BaseVoltage->nominalVoltage.value` |
| baseKV | `GeneratingUnit.RotatingMachine.ratedU` |

Aus der zugehörigen Generator Data Information:

| Generator | CIM-Attribut |
|-----------|--------------|
| Pg        | `GeneratingUnit.initialP` |
| Vg        | `GeneratingUnit.RotatingMachine.RegulatingControl.targetValue` = Vg*baseKV |
| Pg        | Vom zug. `SvPowerFlow` die `p.value`  mit negativem Vorzeichen|
| Vg        | Vom zug. `SvVoltage` die `v.value` auf Vg*baseKV |
| mBase     | `GeneratingUnit.RotatingMachine.ratedS` |

Falls `Pd != 0` oder `Qd != 0`:

- Es muss neben dem Generator auch ein EnergyConsumer erstellt werden (Vorgehen analog zu dem für Typ1)

Falls `Gs != 0` oder `Bs != 0`:

- Es muss auch ein EquivalentShunt erzeugt werden (Vorgehen analog zu dem für Typ1)

### Typ 3 (Ref Bus)
- TopologicalNode erstellen
- ExternalNetworkInjection erstellen und mit TopologicalNode verbinden
- SvVoltage erstellen und mit TopologicalNode verbinden

Aus der Bus-Information:

| Reference bus | CIM-Attribut |
|---------------|--------------|
| baseKV        | Von zugehörigen `TopologicalNode` die `BaseVoltage->nominalVoltage.value` |
| Vm        | `ExternalNetworkIjection.RegulatingControl.targetValue` | 
| Va        | bisher Parametrisierung in Komponente unklar |
| Vm        | Vom zug. `SvVoltage` die `v.value` |
| Va        | Vom zug. `SvVoltage` die `angle.value` |

Falls Generator am betrachteten Bus existiert, werden vorhandene Werte überschrieben:

| Generator | CIM-Attribut |
|-----------|--------------|
| Vg        | `ExternalNetworkIjection.RegulatingControl.targetValue` | 
| Vg        | Vom zug. `SvVoltage` die `v.value` |

Notizen: 
    - Generator.Vg statt Bus.Vm verwenden, da Bus.Vm nicht in allen MPC-Dateien (z.B. in case9) entsprechend der Generator.Vg gesetzt wurde.    

Falls `Gs != 0` oder `Bs != 0`:

- Es muss auch ein EquivalentShunt erzeugt werden (Vorgehen analog zu dem für Typ1)