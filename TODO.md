# TODO

## Open
- [ ] SVPowerFlow of SynchronousMachine should be created with negative p value 
- [ ] Implement functionality of having both; setting component attributes AND state variables (configurable by config file)
- [ ] Convert following MPCs to CIM:
  - case145
  - case300
  - case1354pegase
  - case2869pegase

## Pending 

- [ ] Set for values also UnitMultiplier (e.g. for ratedS of PowerTransformerEnd) 


## For jdi
- [ ] Checker whether anything of the following needed:
    - [ ] Type 1 Bus: Gs, Bs, area number, Vm, Va , zone missing
    - [ ] Type 2 Bus: Gs, Bs, area number, Vm, Va, Pd, Qd
    - [ ] Generator connected to  Bus 2: Qg, Qmax, Qmin, status, Pmax, Pmin missing
    - [ ] Type 3 Bus: Gs, Bs, area number, Vm, Va, Pd, Qd
    - [ ] Generator connected to  Bus 3: Qg, Qmax, Qmin, status, Pmax, Pmin, mBase, Pg missing
    - [ ] Type 4 Bus Missing

## Completed
- [X] Typ 2 (SynchronGenerator):
    - Transformer (Line mit ratio != 0)
        - Folgendes kann ich nicht in den XML wiederfinden: "Erstelle PowerTransformerEnd mit endNumber = 2 ("PowerTransformerEnd_2")", vermutlich wird deswegen im CIMverter auch Vnom2 der Transformatoren nicht gesetzt (in den Modelica-Dateien von case14 steht immer Vnom2 = 0 bei den Transformatoren)
        - Aktuell wird scheinbar zweimal der gleiche Knoten eingelesen, sodass Vnom1=Vnom2 ist
- [x] per-unit Spannungs-Setpoint muss mit Basis-Spannung multipliziert werden, d.h.
    - GeneratingUnit.RotatingMachine.RegulatingControl.targetValue = Vg*baseKV
    - Vom zug. SvVoltage die v.value auf Vg*baseKV
    - siehe auch geupdatete [mapping_matpowercase_cim_classes.md](mapping_matpowercase_cim_classes.md)
- [x] - Setze RatioTapChanger->step auf ratio (entferne Bedingung, dass ratio != 1 muss)
        - Überprüfe, dass `Transformer CIM_TN7_T3PwerTrafo_7_8(TapPos = 0.000000)`
- [x] Connection.cfg mit PVNode nur für ModPowerSystems
- [x] Überprüfe, dass alle Komponenten in der mo-Datei instantiiert wurden (PVNode + EnergyConsumer)
- [x] Setze Standard-Wert im CIMverter von Imax der PiLines auf 100
    - siehe `PiLine CIM_TN10_T3PiLine_10_11(Imax(displayUnit = "A") = 0.000)`
- [x] Überprüfe Setzen von Transformator-Scheinleistung
    - Sollte in der mo-Datei sichtbar sein unter z.B. `Transformer CIM_TN4_T5PwerTrafo_4_7(Sr(displayUnit = "W") = 0.000)`
- [x] Berücksichtigung der Änderungen in Matpower to CIM Mapping (z.B. auch setzen von SV-Objekten für Generator)
- [x] Send jdi xml and mo output (mit und ohne gesetzten SV-Flag)
- [x] case9SvFalse: GeneratingUnit.initialP fehlt
- [x] E-Mail an Max (Lukas in cc) zur Unterstützung der Serialisierung von RatioTapChanger
- [x] Entferne .idea und data/matFiles aus repo
- [x] Anpassen von Source-Code zur Berücksichtigung der Flags
- [x] Verwendung von Konfig-Datei zum Setzen der Flags `useSVforEnergyConsumer`, `useSVforGeneratingUnit`, `useSVforExternalNetworkInjection`
- [x] case9SvTrue: IdentifiedObject.name in EnergyConsumer fehlt
- [x] Dokumentieren der useSv-Flags in Form von Kommentaren in der Konfig-Datei
- [x] Push .mat cases to matpower repo and 
- [x] Use test cases from fork and not from original matpower repo
   - [x] case9: no transformers included (modelled as inductors)
   - [x] case14: transformers with RatioTapChangers
- [x] check commit of submodule libcimpp Serialisation
- [x] Include [fork of matpower](https://git.rwth-aachen.de/acs/core/simulation/matpower) as submodule
    - Use latest commit of branch `mpc2cim`
- Following implemented mappings are incomplete (for sure):
    - [x] Branches: unit conversions and rateA/B/C missing 
    - [x] Type 1-3 Bus not all attributes specified (not sure which are needed, see below)
- [X] Get familiar with MATPOWER case file format (so-called MATPOWER m-files, i.e. MATLAB files)
- [X] Check if there is a MATLAB files parser for C++ (if not, check for which languages you find one)
- [X] Try to use mpc2mat.m in Octave (from the bash), if yes then docuemnt it in the README.md
- [X] Move matpower into parser subdirectory
- [X] Eine Möglichkeit zur Visualisierung von MATPOWER m-Files suchen (vllt. geht das schon direkt mit MATPOWER-Funktionalität unter MATLAB)
- [X] Bei Lukas melden
- [X] Skript für das automatische Umwandeln mithile von `mpc2mat(yourMpcFile, 'yourMpcOutName.mat')` in Octave **aus der bash heraus** erstellen
- [X] Bus vom Typ PQ verarbeiten zu TopologicalNode mit PQLoad
- [X] Add comments to mpc2mat file script
- [X] mpc2mat script  ignore all gencosts
- [X] usage meldungen --help
- [X] debug case 1888 rte


## Bemerkungen
- Notizen zu Test-Cases:
    - case9: keine Transformatoren vorhanden (modelliert als Induktiväten), Bus-Nummern im Vergleich zu NEPLAN vertauscht, im Original waren falsche Basis-Spannungen eingetragen
    - case14: Transformatoren mit RatioTapChangers, im Original waren keine Basis-Spannungen eingetragen
    - case118: Transformatoren mit RatioTapChangers, Basis-Spannungen eingetragen
    - case145: Transformatoren mit RatioTapChangers, Basis-Spannungen eingetragen
- Visualisierung mpc:<br>
  http://immersive.erc.monash.edu.au/stac/

# Sources
## MATPOWER
- MATPOWER case file format: http://www.pserc.cornell.edu/matpower/docs/ref/matpower5.0/caseformat.html
- Example MATPOWER case files: https://github.com/MATPOWER/matpower/tree/master/data

## CIM
- Docs about CIM objects in C++: https://cim.fein-aachen.org/libcimpp/doc/IEC61970_16v29a_IEC61968_12v08/annotated.html
