# Welcome to the mat2CIM Repository
This Repository provides the tools to read Mat Power Case (MPC) files into C++ and convert them to the CIM standard. 

## Conversion posibilities

![Overview Conversion Possibilities](Overviewmat2CIMInputs.svg)

## Dependencies
cmake >=3.5  
clang  
Boost >= 1.60.0  
ctemplate >= 2.3  
libconifg++  
as submodule: libcimpp with arabica  

## Windows

You can run mat2CIM using Docker for Windows:

1. First, you need to install [Docker](https://docs.docker.com/docker-for-windows/install/).
2. Build the mat2CIM docker image:
`docker image build -t mat2CIM .`
3. Start the mat2CIM docker container and mount mat2CIM directory (in which you should put additionally the files to be converted):
`docker run -it --name mat2CIM -v ${pwd}:/data mat2CIM bash`


## User Instructions

mat2CIM currently supports a variety of input formats. Depending on the type of your input file the steps needed to execute mat2CIM differ.  
The graphic above depics the supported input formats as well as assigning them a number. Dependend on the number of your input format continue with one of the following steps:

#### Input Format case 1 mpc.m & case 2 mpc.mat 

Before starting mat2CIM one need to convert the input file to a standardised file which can be inputed to mat2CIM.
Ensure that you are located in the mat2CIM Folder and run:

```sh
octave tools/mpc2mat.m ../yourInputFile.m YourStandarizedInputFile.mat

```
Now that we have a standardized file, we can use it in mat2CIM to generate an xml with the equivalent CIM components:
```sh
./build/bin/mat2CIM -f YourStandarizedInputFile.mat -o outputName
```
The generated outputName.xml will be listed in your the mat2CIM folder.
To move the created xml files to your data directory run:
```sh
mv outputName* ../data/
```


#### Input Format case 3 Pypowercase.py

Before starting mat2CIM one need to convert the input file to a standardised file which can be inputed to mat2CIM.
Ensure that you are located in the mat2CIM Folder and run:

```sh
./tools/pypowerConverter.py /Path/to/YourFile.py
octave tools/mpc2mat.m YourFile.m YourStandarizedInputFile.mat

```
Now that we have a standardized file, we can use it in mat2CIM to generate an xml with the equivalent CIM components:
```sh
./build/bin/mat2CIM -f YourStandarizedInputFile.mat -o outputName
```
The generated outputName.xml will be listed in your the mat2CIM folder.
To move the created xml files to your data directory run:
```sh
mv outputName* ../data/
```

### Setup
First, clone the repository by typing 

```sh
git clone --recursive <git URL>
```

We expect the user to have .m files in MPC standard. 
Before this files can be read into C++  with our software, the user needs to convert them in a .mat file with the mpc2mat tool located in /tools.

Therefore, the user needs to install either MATLAB or the free alternative Octave.

### Convert Files

To convert the yourFile.m into yourFile.mat open a terminal. 
Navigate to the repo folder.
As you can see below, we pass 3 arguments to octave. 
First, the location of the script that is runned.
Second, the input file which should be a .m file in the MPC. 
We expect you to locate your MPC files in REPO/data/mpc. 
Third, the output files which are expected to be located in REPO/data/matFiles.

To convert some .m file in MPC to some .mat file type: 
```sh
octave tools/mpc2mat.m data/mpc/yourFile.m data/matFiles/yourFile.mat

```

### Config 
In CIM, some components can be realized in multiple ways. E.g. a EnergyConsumer can have a p and q value or the terminal connected to the EnergyConsumer can have an SVPowerFlow which stores these values.
Both variants are valid but depending on the toolchain used afterwards one might not be supported.  
Therefore, we introduce Variables which let you choose if you want to generate SVVoltages and SVPowerFlows for different components (TRUE),
if you want them not to be generated (FALSE)(the information is instead stored in the respective component), or if you want the information stored in the component and additonally generate an SV component (BOTH).  
You can change the values in the config.cg. 
The options currently are:  
useSVforEnergyConsumer  
useSVforGeneratingUnit  
useSVforExternalNetworkIjection

## Developer Instructions
### Building and using the software


## Windows

You can develop mat2CIM using Docker for Windows:

1. First, you need to install [Docker](https://docs.docker.com/docker-for-windows/install/).
2. Navigate to the Docker-dev directory
3. Build the mat2CIM docker image:
`docker build -f Dockerfile-dev . -t mat2CIM-dev 
`
4. Start the mat2CIM docker container and mount mat2CIM/src directory (in which you modify your Code):
`
docker run -it --name mat2CIM-dev -v ${pwd}:path/to/mat2CIM/src:/mat2CIM-dev/src mat2CIM-dev bash
`
navigate to the Docker-dev directory.
Build the docker Image 

To create the build directory open a console. 
Navigate to the mat2CIM repo and type:

```sh
mkdir build
```

To compile the software go into the build folder and compile the project : 

```sh
cd build 
cmake ..
make -j$(nproc)
```
A 'bin' folder should be created which contains the executable.

To run the software, navigate to build/bin and run the tool by typing:
```sh
cd build/bin
./mat2CIM -f yourFileLocation.mat -o yourOutputFile
```
