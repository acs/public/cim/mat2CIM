#String literal used to define the beginning and ending of the objects in the input file stored as object.
RULES = {
	"parser" : {
		"ObjectIdentifiers" : {
			"BeginningData": {
				"AreaData" : ['%% area data','mpc.areas = \\[','mpc.areas= \\[','mpc.areas= \\['],
				"BusData" : ['%% bus data','mpc.bus = \\[','mpc.bus= \\[','mpc.bus =\\['],
				"GeneratorData" : ['%% generator data','mpc.gen = \\[','mpc.gen= \\[','mpc.gen =\\['],
				"GeneratorCostData" : ['mpc.gencost = \\[','mpc.gencost= \\[','mpc.gencost =\\['],
				"BranchData" : ['%% branch data','mpc.branch = \\[','mpc.branch= \\[','mpc.branch =\\['],
				"BusLocation" : ['%% bus location','%% bus location','mpc.buslocation = \\['],
				"BaseMVA" : ['mpc.baseMVA = ','mpc.baseMVA	= ']
			},
			"EndingData": {
				"AreaData" : "];",
				"BusData" : "];",
				"GeneratorData" : "];",
				"GeneratorCostData" : "];",
				"BranchData" : "];",
				"BusLocation" : "];",
				"BaseMVA" : ";"
			}
		},
		"startIndexIdentifire" : [
		'mpc.areas = [','mpc.areas= [','mpc.areas= [','mpc.bus = [','mpc.bus= [','mpc.bus =[','mpc.gen = [','mpc.gen= [','mpc.gen =[','mpc.gencost = [','mpc.gencost= [','mpc.gencost =[','mpc.branch = [','mpc.branch= [','mpc.branch =[','mpc.buslocation = ['
		],
		"HardCodedDefaultProperties": {
			"BusLocation" : ['bus_i','x','y'],
			"AreaData" : ['AreaData'],
			"BusData" : ["bus_i","type","Pd","Qd","Gs","Bs","area","Vm","Va","baseKV","zone","Vmax","Vmin"],
			"GeneratorData" : ["bus","Pg","Qg","Qmax","Qmin","Vg","mBase","status","Pmax","Pmin","Pc1","Pc2","Qc1min","Qc1max","Qc2min","Qc2max","ramp_agc","ramp_10","ramp_30","ramp_q","apf"],
			"GeneratorCostData" : ["GenID", "startup", "shutdown", "n", "cost1", "cost2", "cost3"],
			"GeneratorCostDataLinear" : ["GenID", "startup", "shutdown", "n", "cost2", "cost3"],
			"BranchData" : ["fbus","tbus","r","x","b","rateA","rateB","rateC","ratio","angle","status","angmin","angmax"]			
		},
	},
	"dataHeaders" : {
		"BeginningData": {
			"AreaData" : ['%% area data','mpc.areas = \\[','mpc.areas= \\[','mpc.areas= \\['],
			"BusData" : ['%% bus data','mpc.bus = \\[','mpc.bus= \\[','mpc.bus =\\['],
			"GeneratorData" : ['%% generator data','mpc.gen = \\[','mpc.gen= \\[','mpc.gen =\\['],
			"GeneratorCostData" : ['%% generator cost data','mpc.gencost = \\[','mpc.gencost= \\[','mpc.gencost =\\['],
			"BranchData" : ['%% branch data','mpc.branch = \\[','mpc.branch= \\[','mpc.branch =\\['],
			"BusLocation" : ['%% bus location','%% bus location','mpc.buslocation = \\['],
			"BaseMVA" : ['mpc.baseMVA = ','mpc.baseMVA	= ']
		},
	},


	"nodeToolTip" : [
		{"key":"ToolTipTitle", "data":"bus_i","units":"-NA-","preTitleValText":"Bus ","postTitleValText":""},
		{"key":"Id", "data":"bus_i","units":"-NA-","nature":"static"},
		{"key":"V Max", "data":"Vmax","units":"Volts p.u.","nature":"static"},
		{"key":"V Min", "data":"Vmin","units":"Volts p.u.","nature":"static"},
		{"key":"Voltage", "data":"Vm","units":"Volts p.u.","nature":"dynamic"},
		{"key":"Phase Angle", "data":"Va","units":"Degrees","nature":"dynamic"},
		{"key":"Base KV", "data":"baseKV","units":"KV","nature":"static"},
		{"key":"Generator Id(s) List", "data":"GenIdList","units":"-NA-","nature":"static"},
	],

	"bottomDecoToolTipLoad" : [
		{"key":"ToolTipTitle", "data":"bus_i","units":"-NA-","preTitleValText":"Loads on Bus ","postTitleValText":""},
		{"key":"P", "data":"Pd","units":"MW"},
		{"key":"Q", "data":"Qd","units":"MVAr"},
	],

	"bottomDecoToolTipShunt" : [
		{"key":"ToolTipTitle", "data":"bus_i","units":"-NA-","preTitleValText":"Loads on Bus ","postTitleValText":""},
		{"key":"G", "data":"Gs","units":"MW demanded at V = 1.0 p.u."},
		{"key":"B", "data":"Bs","units":"MVAr injected at V = 1.0 p.u."},
	],

	"bottomDecoToolForValidation" : [
		{"key":"ToolTipTitle", "data":"bus_i","units":"-NA-","preTitleValText":"Loads on Bus ","postTitleValText":""},
		{"key":"P", "data":"Pd","units":"MW"},
		{"key":"Q", "data":"Qd","units":"MVAr"},
		{"key":"G", "data":"Gs","units":"MW demanded at V = 1.0 p.u."},
		{"key":"B", "data":"Bs","units":"MVAr injected at V = 1.0 p.u."},
	],

	"topDecoToolTip" : [
		{"key":"ToolTipTitle", "data":"id","units":"-NA-","preTitleValText":"Generator ","postTitleValText":""},
		{"key":"Bus Id", "data":"bus","units":"-NA-"},
		{"key":"P", "data":"Pg","units":"MW","nature":"dynamic"},
		{"key":"P Min Bounds", "data":"Pmin","units":"MW","nature":"static"},
		{"key":"P Max Bounds", "data":"Pmax","units":"MW","nature":"static"},
		{"key":"Q", "data":"Qg","units":"MVAr","nature":"dynamic"},
		{"key":"Voltage", "data":"Vg","units":"Volts p.u.","nature":"dynamic"},
		{"key":"Q Min Bounds", "data":"Qmin","units":"MVAr","nature":"static"},
		{"key":"Q Max Bounds", "data":"Qmax","units":"MVAr","nature":"static"},
		{"key":"Cost 1", "data":"costData.cost1","units":"$/MW hr squared"},
		{"key":"Cost 2", "data":"costData.cost2","units":"$/MW hr"},
		{"key":"Cost 3", "data":"costData.cost3","units":"$/MW hr"},
	],

	"edgeToolTip" : [
		{"key":"ToolTipTitle", "data":"edgeId","units":"-NA-","preTitleValText":"Line ","postTitleValText":""},
		{"key":"Id", "data":"index","units":"-NA-"},
		{"key":"r", "data":"edgeData.r","units":"Resistance p.u."},
		{"key":"x", "data":"edgeData.x","units":"Reactance p.u."},
		{"key":"charge", "data":"edgeData.b","units":"Susceptance p.u."},
		{"key":"Rate A", "data":"edgeData.rateAToolTip","units":"MVA"},
		{"key":"Rate B", "data":"edgeData.rateBToolTip","units":"MVA"},
		{"key":"Rate C", "data":"edgeData.rateCToolTip","units":"MVA"},
		{"key":"Min angle difference", "data":"edgeData.angmin","units":"Degrees"},
		{"key":"Max angle difference", "data":"edgeData.angmax","units":"Degrees"},

		{"key": "Active power forward",
			"data":"solutionData.p-s-t","units":"MW","nature":"dynamic"},
		{"key":"Reactive power forward",
			"data":"solutionData.q-s-t","units":"MVAr","nature":"dynamic"},
		{"key":"Active power reverse",
			"data":"solutionData.p-t-s","units":"MW","nature":"dynamic"},
		{"key":"Reactive power reverse",
			"data":"solutionData.q-t-s","units":"MVAr","nature":"dynamic"},
		{"key":"Apparent power forward",
		"data":"solutionData.s-s-t","units":"MVA","nature":"dynamic"},
		{"key":"Apparent power reverse",
		"data":"solutionData.s-t-s","units":"MVA","nature":"dynamic"},
		{"key":"Angle Difference","data":"solutionData.angleDiffVal","units":"Degrees","nature":"dynamic"},
	],

	"transformerEdgeToolTipExtra" : [
		{"key":"Transformer Tap", "data":"edgeData.ratio","units":"Volts p.u."},
		{"key":"Phase Shift", "data":"edgeData.angle","units":"Degrees"},
	],
};




WARNINGS = {
	"lineWarnings" : [
		{"wID":"LINE.WARNING.R", "template":"1","eleId":"","attrName":"","domEleID":""},
	],
}

LOGGING = {
	"TEMPLATES" : [
		{"id":"1","text":"The Value of '%attrName%', for the element '%eleId%' is 'less than zero'."},
		{"id":"2","text":"The Value of '%attrName%', for the element '%eleId%' is 'greater than zero'."},
	],
}