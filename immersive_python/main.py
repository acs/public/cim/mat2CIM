FILE = open("case1354pegase.m","r")

FILE = FILE.read()
from ObjectFactory.dataObjects import ObjectFactory as ObjectFactory
from ObjectFactory.dataObjSolutionUpdater import Solution
from ObjectFactory.dataObjValidator import Validator
from utils.Rules import RULES

objFac = ObjectFactory(3,4,FILE)
NETWORK_OBJECTS = objFac.getNetworkDataObjects()
NETWORK = objFac.getNetwork()
#print (NETWORK)
SOL = Solution(NETWORK_OBJECTS, NETWORK)
NETWORK_OBJECTS = SOL.getNetworkObjects()

dataValidator = Validator(NETWORK_OBJECTS, NETWORK)
dataValidator.validateEdges()
dataValidator.validateNode()



