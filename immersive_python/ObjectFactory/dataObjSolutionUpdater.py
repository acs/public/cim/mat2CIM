import math

class Solution():
    NETWORK = {}
    def __init__(self, networkObj, NETWORK):
        self.networkObj = networkObj
        self.NETWORK = NETWORK
        busCount = len(networkObj['busDataObj']['dataObjList']) # .dataObjList
        for indexBus in range(0, busCount):
            self.networkObj['busDataObj']['dataObjList'][indexBus] = self.addNodeSolutionData(self.networkObj['busDataObj'][indexBus]) # .dataObjList


        branchCount = len(self.networkObj['branchDataObj']['dataObjList']) #length;

        for branchIndex in range(0, branchCount):
            #edgeData = self.networkObj['branchDataObj']['dataObjList'][branchIndex]['edgeData']
            #nodeIndexMap = self.NETWORK['nodeEdgeMap']
            self.networkObj['branchDataObj']['dataObjList'][branchIndex] =\
                self.addEdgeSolutionData(self.networkObj['branchDataObj']['dataObjList'][branchIndex])
        #return self.networkObj

    def getNetworkObjects(self):
        return self.networkObj


    def addEdgeSolutionData(self, edgeObj):

        nodeIndexMap = self.NETWORK["nodeEdgeMap"];
        edgeData = edgeObj['edgeData']

        source = self.networkObj['busDataObj'][nodeIndexMap[int(edgeData['fbus'])]]
        target = self.networkObj['busDataObj'][nodeIndexMap[int(edgeData['tbus'])]]
        solData = {}
        solData["angleDiffVal"] = float(float(source['Va']) - float(target['Va']))

        solData["angle"] = (float(edgeData['angle']) * math.pi / 180)
        solData["r"] = float(edgeData['r']);
        if (float(edgeData['ratio']) == 0.0):
            solData["ratio"] = 1.0;
        else:
            solData["ratio"] = float(edgeData['ratio']);
        r = float(edgeData['r'])
        rSquare = math.pow(r, 2)
        x = float(edgeData['x'])
        xSquare = math.pow(x, 2);
        solData["b"] = -x / (rSquare + xSquare);
        solData["g"] = r / (rSquare + xSquare);

        a1 = float(solData['g']) * math.pow((float(source['Vm']) / float(solData['ratio'])), 2);

        a2 = float(solData['g']) * (float(source['Vm']) / float(solData['ratio'])) * float(
            target['Vm']) * math.cos(source['solData']['Va'] - target['solData']['Va'] - solData['angle'])

        a3 = float(solData['b']) * float(source['Vm']) / float(solData['ratio']) * float(
            target['Vm']) * math.sin(source['solData']['Va'] - target['solData']['Va'] - solData['angle'])
        solData["p-s-t"] = ((a1 - a2 - a3) * float(self.networkObj['BaseMVA']))

        b1 = (float(edgeData['b']) / 2 + solData['b']) * math.pow((float(source['Vm']) / float(solData['ratio'])),
                                                                 2);
        b2 = solData['b'] * (float(source['Vm']) / float(solData['ratio'])) * float(target['Vm']) * math.cos(
            source['solData']['Va'] - target['solData']['Va'] - solData['angle'])

        b3 = solData['g'] * (float(source['Vm']) / solData['ratio']) * float(target['Vm']) * math.sin(
            source['solData']['Va'] - target['solData']['Va'] - solData['angle']);
        solData["q-s-t"] = ((-b1 + b2 - b3) * float(self.networkObj['BaseMVA']))

        c1 = solData['g'] * math.pow(float(target['Vm']), 2)

        c2 = solData['g'] * float(source['Vm']) * (float(target['Vm']) / float(solData['ratio'])) * math.cos(
            target['solData']['Va'] - source['solData']['Va'] + solData['angle'])

        c3 = solData['b'] * float(source['Vm']) * (float(target['Vm']) / solData['ratio']) * math.sin(
            target['solData']['Va'] - source['solData']['Va'] + solData['angle']);
        solData["p-t-s"] = ((c1 - c2 - c3) * float(self.networkObj['BaseMVA']))

        d1 = (float(edgeData['b']) / 2 + float(solData['b'])) * math.pow(float(target['Vm']), 2);

        d2 = float(solData['b']) * float(source['Vm']) * (float(target['Vm']) / float(solData['ratio'])) * math.cos(
            target['solData']['Va'] - source['solData']['Va'] + solData['angle'])

        d3 = float(solData['g']) * float(source['Vm']) * (float(target['Vm']) / solData['ratio']) * math.sin(
            target['solData']['Va'] - source['solData']['Va'] + solData['angle'])
        solData["q-t-s"] = ((-d1 + d2 - d3) * float(self.networkObj['BaseMVA']))

        solData["s-s-t"] = math.sqrt(math.pow(solData["p-s-t"], 2) + math.pow(solData["q-s-t"], 2))

        solData["s-t-s"] = math.sqrt(math.pow(solData["p-t-s"], 2) + math.pow(solData["q-t-s"], 2));

        edgeObj["solutionData"] = solData;
        return edgeObj


    def addNodeSolutionData(self, nodeObj):
        solData = {}
        vaSol = (float(nodeObj['Va']) * math.pi / 180)
        solData["Va"] = vaSol
        nodeObj["solData"] = solData

        # Va[i] = Va[i] * pi / 180 - convert bus phase angles to radians
        return nodeObj


