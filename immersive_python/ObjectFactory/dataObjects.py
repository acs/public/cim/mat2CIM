#package ObjectFactory/dataObjects
import sys
import re
import json
import math
sys.path.append("..")
from utils.Rules import *


class ObjectFactory:

    GLOBAL_FILE = []
    NETWORK = {}
    OBJECTS = []


    def __init__(self, a , b, FILE):
        self.a = a
        global GLOBAL_FILE
        GLOBAL_FILE = FILE

        self.b = b
        self.OBJECTS = self.dataObjects(self.infoForObjectGeneration(FILE), FILE)
        self.OBJECTS = self.addTopDecoratorDataToBus(self.OBJECTS)
        self.OBJECTS = self.addBottomDecoratorDataToBus(self.OBJECTS)
        self.OBJECTS = self.updateEdgesData(self.OBJECTS)
        self.OBJECTS = self.updateBusStatus(self.OBJECTS)



    def getNetworkDataObjects(self):
        return self.OBJECTS

    def getNetwork(self):
        return self.NETWORK


    def infoForObjectGeneration (self, FILE):
        _lclObjBeginIdentifiers = RULES["parser"]["ObjectIdentifiers"]["BeginningData"]
        _lclObjEndIdentifiers = RULES["parser"]["ObjectIdentifiers"]["EndingData"]
        allObjectInfo = []

        for key in _lclObjBeginIdentifiers:
            name = key;
            searchParamsArray = _lclObjBeginIdentifiers[key]
            for i in range(0,len(searchParamsArray)):
                #if (FILE.search(searchParamsArray[i].toString()) != -1):
                if (FILE.find( searchParamsArray[i]) != -1):
                    if (i == 1):
                        lineGap = 2
                    else:
                        lineGap = 1

                    beginLine = FILE.find(searchParamsArray[i])
                    endLine = FILE.find(_lclObjEndIdentifiers[key], beginLine)

                    isPropNamesPresent = True
                    if (searchParamsArray[i] == "mpc.buslocation = \\["):
                        isPropNamesPresent = False;
                        lineGap = 0;
                    objInfo = {"name": name, "beginChar": beginLine, "endChar": endLine, "lineGap": lineGap,
                   "isPropNamesPresent": isPropNamesPresent}
                    allObjectInfo.append(objInfo);
                    break

        return allObjectInfo


    def beautifyValue(self, strVal):
        updatedStrVal = strVal.replace(';\r', '')
        updatedStrVal = updatedStrVal.replace('\r', '')
        updatedStrVal = updatedStrVal.strip()
        if (updatedStrVal.find(";") !=  -1):
            updatedStrVal = updatedStrVal[0 : updatedStrVal.find(";")]
        return updatedStrVal



    def dataObjects(self, dictObj,file):
        infoObject = dictObj;
        networkConfig = {};
        networkConfig["areaData"] = {}
        networkConfig['busDataObj'] = {}
        networkConfig["generatorDataObj"] = {}
        networkConfig["generatorCostDataObj"] = {}
        networkConfig["branchDataObj"] = {}
        networkConfig["busLocation"] = {}
        networkConfig["BaseMVA"] = {}
        for item in infoObject:
            if(item["name"] == 'AreaData'):
                areaData = self.generateDataObject(item, file)
                networkConfig["areaData"] = areaData
            elif(item["name"] == 'BusData' ):
                busData = self.generateDataObject(item, file)
                networkConfig['busDataObj'] = busData
            elif(item["name"] == 'GeneratorData' ):
                genDataObj = self.generateDataObject(item, file)
                networkConfig["generatorDataObj"] = genDataObj
            elif(item["name"] == 'GeneratorCostData' ):
                genCostDataObj = self.generateDataObject(item, file)
                networkConfig["generatorCostDataObj"] = genCostDataObj
            elif(item["name"] == 'BranchData' ):
                branchDataObj = self.generateDataObject(item, file)
                networkConfig["branchDataObj"] = branchDataObj
            elif(item["name"] == 'BusLocation' ):
                busLocation = self.generateDataObject(item, file)
                networkConfig["busLocation"] = busLocation
            elif(item["name"] == 'BaseMVA' ):
                networkConfig["BaseMVA"] = str(self.beautifyValue(str(self.getObjectContent(item, file))).split("=")[1]);
        return networkConfig





    def addTopDecoratorDataToBus(self, networkObjects):
        for i in range(0, len(networkObjects['busDataObj']['dataObjList'])):
            busObj = networkObjects['busDataObj']['dataObjList'][i]
            # As of now only the ID is added to the generator data object - the data added needs to updated.- 16 / 12 / 2014.
            topDecorators = [] #{}
            topDecoratorsID = "";
            for j in range(0, len(networkObjects['generatorDataObj']['dataObjList'])):
                genObj = networkObjects['generatorDataObj']['dataObjList'][j]
                if len(genObj) > 0:
                    if (genObj['bus'] == busObj['bus_i']):
                        # As the values are dynamically assigned thus declaring the object outside the for loop will cause a reference error and only the value of the last object will be stored.
                        actualDataObj = {}
                        id = (j + 1)
                        actualDataObj["id"] = id
                        genObj['Pd'] = busObj['Pd']
                        genObj["Qd"] = busObj['Qd']
                        genObj["id"] = id
                        topDecoratorsID = topDecoratorsID + str(id) + ","
                        if (genObj['Pmax'] == 0 and genObj['Pmin'] == 0):
                            actualDataObj["type"] = "synCondensor"
                            actualDataObj["text"] = "c"
                        else:
                            actualDataObj["type"] = "generator"
                            actualDataObj["text"] = "~"
                        # Adding the DOMID to the top decorators.- This is the id of the top decorator group.
                        genObj["DOMID"] = ("bus" + busObj['bus_i'] + "topDeco")
                        # Also the same DOMID is added to the decorator group element so as to avoid any error.( in future code implementation).
                        #topDecorators["DOMID"] = ("bus" + busObj['bus_i'] + "topDeco")

                        actualDataObj["topDecoData"] = genObj
                        topDecorators.append(actualDataObj)

            busObj["topDecorators"] = topDecorators
            busObj["GenIdList"] = topDecoratorsID[0 : -1]

            networkObjects['busDataObj'][i] = busObj
        return networkObjects


    def addBottomDecoratorDataToBus(self, networkObjects):

        for i in range(0, len(networkObjects['busDataObj']['dataObjList'])):
            busObj = networkObjects['busDataObj']['dataObjList'][i]
            bottomDecorators =[]


            if ((float(busObj['Pd']) !=  0.0) or (float(busObj['Qd']) !=  0.0)):
                # busType = load;
                lObj = {}
                lObj["type"] = "load"
                lObj["Pd"] = busObj['Pd']
                lObj["Qd"] = busObj['Qd']
                lObj["Gs"] = busObj['Gs']
                lObj["Bs"] = busObj['Bs']
                # Adding the DOMID to the bottom decorator - this refers to the ID of the bottom group element.
                lObj["DOMID"] = ("bus" + busObj['bus_i'] + "bottomDeco")
                bottomDecorators.append(lObj)


            if ((float(busObj['Gs']) != 0.0) or (float(busObj['Bs']) != 0.0)):
                # busType = "shunt";
                sObj = {}
                sObj["type"] = "shunt"
                sObj["Pd"] = busObj['Pd']
                sObj["Qd"] = busObj['Qd']
                sObj["Gs"] = busObj['Gs']
                sObj["Bs"] = busObj['Bs']
                # Adding the DOMID to the bottom decorator - this refers to the ID of the bottom group element.
                sObj["DOMID"] = ("bus" + busObj['bus_i'] + "bottomDeco")
                bottomDecorators.append(sObj)

            # Adding the DOMID to the bottom decorator group
            #bottomDecorators["DOMID"] = ("bus" + busObj['bus_i'] + "bottomDeco")
            busObj["bottomDecorators"] = bottomDecorators
            networkObjects['busDataObj'][i] = busObj
        return networkObjects




    def updateEdgesData(self, networkConfigObj):
        edges = {}
        nodeIndexMap = {}
        nodeObjectMap = {}
        edgeMapForMultiLine = [];

        for nodeIndexer in range(0, len(networkConfigObj['busDataObj']['dataObjList'])):#['dataObjList']
            nodeIndexMap[int(networkConfigObj['busDataObj']['dataObjList'][nodeIndexer]['bus_i'])] = nodeIndexer;


        # Adding Node Branch Map to the NETWORK - This has been added to NETWORK because once
        # created it is independent of the Data Object

        self.NETWORK["nodeEdgeMap"] = nodeIndexMap


        for branchIndexer in range(0, len(networkConfigObj['branchDataObj']['dataObjList'])):
            #['dataObjList']
            edgeDataObj = networkConfigObj['branchDataObj']['dataObjList'][branchIndexer]
            #['dataObjList']

            edgeType = "Standard"
            if (float(edgeDataObj['ratio']) != 0.0 or float(edgeDataObj['angle']) != 0.0):
                edgeType = "Transformer"

            elif (float(edgeDataObj['b']) != 0.0):
                edgeType = "LineCharge"

            edgeRepCount = 1
            isMultiLine = False
            edgeName = edgeDataObj['fbus'] + "-" + edgeDataObj['tbus'] + "-" + edgeDataObj['tbus'] + "-" + edgeDataObj['fbus'] + "-" + str(edgeRepCount)


            # Checking if the name of the edge has already been added to the array.
            #if ($.inArray(edgeName, edgeMapForMultiLine) != -1):
            if not edgeName in edgeMapForMultiLine :
                isMultiLine = True
                # Setting Multi line true for all previous repetitive edges.
                #for edges in
                for edge in edges:

                    if(edge[0 : -2] == edgeName):#[0 : -2]
                        edge = isMultiLine
                        edgeRepCount += 1

                edgeName[0 : - 2]
                edgeName = edgeName + "-" + str(edgeRepCount)

            edgeMapForMultiLine.append(edgeName);

            # Region - Add Thermal Rating to the edge
            # Angle to be used to find the Cosine.
            delta = max(float(edgeDataObj['angmin']), float(edgeDataObj['angmax']))
            cosDel = 0
            if (delta > 90):
                cosDel = 0
            else:
                cosDel = math.cos(delta * (math.pi / 180))




            sourceBus = networkConfigObj['busDataObj'][nodeIndexMap[int(edgeDataObj['fbus'])]]#.dataObjList
            targetBus =  networkConfigObj['busDataObj'][nodeIndexMap[int(edgeDataObj['tbus'])]]#.dataObjList

            srcVmSq = math.pow(float(sourceBus['Vmax']), 2)
            trgVmSq = math.pow(float(targetBus['Vmax']), 2)
            ySquared = (1 / ((math.pow(float(edgeDataObj['r']), 2)) + (math.pow(float(edgeDataObj['x']), 2))))
            equSecPart = (srcVmSq + trgVmSq - (2 * float(sourceBus['Vmax']) * float(targetBus['Vmax']) * cosDel))
            V1 = abs(srcVmSq * equSecPart * ySquared)
            V2 = abs(trgVmSq * equSecPart * ySquared)
            UB = math.sqrt(max(V1, V2)) * float(networkConfigObj['BaseMVA'])
            edgeDataObj["UB"]  = UB
            # Region Ends

            # Added to update the value of rateA if it is zero - As advised by Dr.Carleton
            if (edgeDataObj['rateA'] == "0"):
                edgeDataObj["rateAToolTip"] = "none"
                edgeDataObj["rateBToolTip"] = "none"
                edgeDataObj["rateCToolTip"] = "none"
            else:
                edgeDataObj["rateAToolTip"] = edgeDataObj['rateA']
                edgeDataObj["rateBToolTip"] = edgeDataObj['rateB']
                edgeDataObj["rateCToolTip"] = edgeDataObj['rateC']

            edge = {
            "index":branchIndexer + 1,
                    "edgeId": ("From Bus '" + edgeDataObj['fbus'] + "' to Bus '" + edgeDataObj['tbus'] + "'"),
                              "source": nodeIndexMap[int(edgeDataObj['fbus'])],
                                        "target": nodeIndexMap[int(edgeDataObj['tbus'])],
                                                  "edgeData": edgeDataObj,
                                                              "edgeType": edgeType,
                                                                          "edgeName": edgeName,
                                                                                      "isMultiLine": isMultiLine,
            }
            edges[edgeName] = edge


        edgeData = []

        for edge in edges:
            edgeData.append(edges[edge])



        networkConfigObj['branchDataObj']['dataObjList'] = edgeData#.dataObjList
        return networkConfigObj



    def updateBusStatus(self, networkConfigObj):
        for nodeIndexer in range(0, len(networkConfigObj['busDataObj']['dataObjList'])):
            status = 1
            if networkConfigObj['busDataObj']['dataObjList'][nodeIndexer]['type'] ==  "4":
                status = 0
            networkConfigObj['busDataObj']['dataObjList'][nodeIndexer]["status"] = status

        return networkConfigObj


    def getObjectContent(self, rawDataObj, file):
        objects = file[rawDataObj['beginChar'] : rawDataObj['endChar'] + 2].split('\n')
        return objects



    def generateDataObject(self, rawDataObj, file):
        dataObjectWrapper = {}
        dataObjList = [];
        #dataObjectWrapper.dataObjList = dataObjList;
        content = self.getObjectContent(rawDataObj, file);

        #objProperties, i, s, crtContent, propIndexer, actualDataObj, eachObjectData, valIndexer, valStartIndex
        o = RULES['parser']['startIndexIdentifire']

        objProperties = RULES['parser']['HardCodedDefaultProperties'][rawDataObj['name']];
        for i in range(0, len(content)):
            s = content[i]#.replace( / (\r\n | \n | \r) / gm, "");
            len_list = [len(el) for el in o]
            len_min_prefix = min(len_list)
            if(str(o).find(s[0 : s.find("[")]) != -1 and len(s) >= len_min_prefix):# + 1]) != -1):
                valStartIndex = i ;

                # content.length-1 has been taken because the index starts from 0 whereas the length is calculated from 1.
                for valIndexer in range(valStartIndex, len(content) - 1):
                    crtContent = content[valIndexer].strip()
                    #crtContent = $.strip(content[valIndexer])
                    # Updated the condition for the content parsing to include the check for the '%' in the line.The parsing is done only if the first element in the line is not %.
                    if ((crtContent !=  "") and (crtContent.find("%") != 0)):
                        if (crtContent.find(' ') != -1):
                            print ("maybe we need replace here")
                            #print(crtContent)
                            #crtContent = crtContent.replace( / \s{1, } / g, '\t');

                        eachObjectData = crtContent.split('\t');
                        actualDataObj = {};

                        # Special Handling for the generator cost data object.
                        if (rawDataObj['name'] == "GeneratorCostData"):
                            if (parseInt(eachObjectData[3]) == 2):
                                # The object name is used to identify the name of the properties.
                                objProperties = RULES['parser']['HardCodedDefaultProperties']['GeneratorCostDataLinear']


                        created = False
                        for propIndexer in range(0, len(objProperties)):
                            if(len(eachObjectData) != len(objProperties)):
                               # print("problem with obj ")
                                continue;
                            created = True
                            actualDataObj[objProperties[propIndexer]] = \
                                self.beautifyValue((eachObjectData[propIndexer]));
                        if created:
                            dataObjList.append(actualDataObj)
        dataObjectWrapper['dataObjList']= dataObjList

        return dataObjectWrapper


