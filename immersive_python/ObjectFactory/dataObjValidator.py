from utils.Rules import *
import math

class Validator():
    networkObjects = []
    edgeDataObjs = []
    nodeDataObjs = []
    NETWORK = []
    def __init__(self, networkObj, NETWORK):
        self.networkObjects = networkObj;
        self.NETWORK = NETWORK
        self.edgeDataObjs = self.networkObjects['branchDataObj']
        self.nodeDataObjs = self.networkObjects['busDataObj']


    def validateTopDecorator(self, crtNode):
        topDecorators = crtNode['topDecorators'];
        validationWarning = []
        for index in range(0, len(topDecorators)):

            topDeco = topDecorators[index]
            warning = False
            data = topDeco['topDecoData']
            if 'validationWarning' in topDeco:
                warningList = topDeco['validationWarning']
            else:
                warningList = []
            validationWarning.append(RULES['topDecoToolTip'])

            if ((int(data['status']) == 1) and (int(crtNode['status']) == 0)):
                statusWarning = {"key":"Status", "data":"Status is 1 where as Status for Node is 0.", "custom":"true", "type":"warning"}
                warning = True
                validationWarning.append(statusWarning)
                #LOGGER.addWarningMessage(
                print ("Status of Bus " + crtNode.bus_i + " is 0 where as status for Generator " + data.id + " is 1.", data.DOMID,
                    "topDeco")


            if (float(data['Vg']) != float(crtNode['Vm'])):
                VgWarning = {"key":"Vg", "data":"The voltage of generator " + str(data['id']) + " is not equal to voltage of bus " + crtNode['bus_i'] + ".", "custom":"true", "type":"warning"};
                warning = True
                warningList.append("Voltage")
                validationWarning.append(VgWarning)
                print(
                    "The voltage of generator " + str(data['id']) + " is not equal to voltage of bus " + crtNode['bus_i'] + ".",
                    data['DOMID'], "topDeco");




            for i in range (0, len(validationWarning)):
                breakstuff = False
                for war in warningList:
                    if(breakstuff == False):
                        if(type(validationWarning[i]) is list ):
                            for j in range(0, len(validationWarning[i])):
                                if (str(war).find(validationWarning[i][j]['key']) != -1 ):
                                    validationWarning[i][j]["classed"] = "error"
                                    breakstuff = True
                                    break;

            #for i in range(0, len(validationWarning)):
            #    for warning in warningList:
            #        if(warning.find(validationWarning[i]) != -1 ):
            #            validationWarning[i]["classed"] = "warning";


            topDeco["validationWarning"] = validationWarning;
            topDeco["warning"] = warning;


            error = False
            errorList = []
            if ((float(data['Qmin']) > float(data['Qmax'])) or (float(data['Pmin']) > float(data['Pmax']))):
                pgBounds = {"key":"Error", "data":"Infeasible p, g bounds.", "custom":"true", "type":"error"};
                error = True

                if (float(data['Qmin']) > float(data['Qmax'])):
                    errorList.append("Q Min Bounds")
                    errorList.append("Q Max Bounds")


                if (float(data['Pmin']) > float(data['Pmax'])):
                    errorList.append("P Min Bounds")
                    errorList.append("P Max Bounds")

                validationWarning.append(pgBounds);
                print ("Generator " + str(data['id']) + " has infeasible bounds.", data['DOMID'], "topDeco");

            if ((float(data['Qg']) < float(data['Qmin'])) or (float(data['Qg']) > float(data['Qmax']))):
                infeasibleQVal = {"key":"Error", "data":"Q is out of bounds.", "custom":"true", "type":"error"};
                error = True
                errorList.append("Q");
                validationWarning.append(infeasibleQVal);
                print ("Generator " + str(data['id']) + " - Q is out of bounds.", data['DOMID'], "topDeco");


            if ((float(data['Pg']) < float(data['Pmin'])) or (float(data['Pg']) > float(data['Pmax']))):
                infeasiblePVal = {"key":"Error", "data":"P is out of bounds.", "custom":"true", "type":"error"};
                error = True
                errorList.append("P");
                validationWarning.append(infeasiblePVal);
                print ("Generator " , data['id'] , "- P is out of bounds.", data['DOMID'], "topDeco");



            for i in range (0, len(validationWarning)):
                breakstuff = False
                for err in errorList:
                    if(breakstuff == False):
                        if(type(validationWarning[i]) is list ):
                            for j in range(0, len(validationWarning[i])):
                                if (err.find(validationWarning[i][j]['key']) != -1 ):
                                    validationWarning[i][j]["classed"] = "error"
                                    breakstuff = True
                                    break;

            topDeco["validationError"] = validationWarning
            topDeco["error"] = error
            crtNode['topDecorators'][index] = topDeco
        return crtNode




    def validateBottomDecorator(self, crtNode):

        for index in range (0, len(crtNode['bottomDecorators'])):
            data = crtNode['bottomDecorators'][index]

            if 'validationWarning' in data:
                warningList = data['validationWarning']
            else:
                warningList = []
            warning = False
            validationWarning = []

            validationWarning.append(RULES['bottomDecoToolForValidation']);
            if (float(data['Pd']) < 0):
                warning = True;
                warningList.append("P");
                validationWarning.append({"key":"Status", "data":"Negative P value on Bus load.", "custom":"true", "type":"warning"});
                print ("Bus " + crtNode['bus_i'] + " has Negative P value.", data['DOMID'], "bottomDeco");


            if (float(data['Qd']) < 0):
                warning = True
                warningList.append("Q");
                validationWarning.append({"key":"Status", "data":"Negative Q value on Bus load.", "custom":"true", "type":"warning"});
                print ("Bus " + crtNode['bus_i'] + " has Negative Q value.", data['DOMID'], "bottomDeco");

            if (float(data['Gs']) < 0):
                warning = True
                warningList.push("G")
                validationWarning.append({"key":"Status", "data":"Negative G value on Bus shunt.", "custom":"true", "type":"warning"})
                print ("Bus " + crtNode['bus_i'] + " has Negative G value.")

            if (float(data['Bs']) > 0):
                warning = True
                warningList.append("B");
                validationWarning.append({"key":"Status", "data":"Positive B value on Bus load.", "custom":"true", "type":"warning"});
                print ("Bus " + crtNode['bus_i'] + " has Positive B value.");



            for i in range (0, len(validationWarning)):
                breakstuff = False
                for war in warningList:
                    if(breakstuff == False):
                        if(type(validationWarning[i]) is list ):
                            for j in range(0, len(validationWarning[i])):
                                if (str(war).find(validationWarning[i][j]['key']) != -1 ):
                                    validationWarning[i][j]["classed"] = "warning"
                                    breakstuff = True
                                    break;
            #for i in range(0, len(validationWarning)):
                #if ($.inArray(validationWarning[i].key, warningList) != = -1)
            #    validationWarning[i]["classed"] = "warning"

            data["validationWarning"] = validationWarning
            data["warning"] = warning
            crtNode['bottomDecorators'][index] = data
        return crtNode



    def validateEdges(self):
        edgesData = []
        edgesData = self.edgeDataObjs['dataObjList']
        nodeIndexMap = self.NETWORK['nodeEdgeMap']

        for index in range(0, len(edgesData)):
            data = edgesData[index]
            warningList=[]
            warning = False
            error = False
            validationErrorWarning = []
            validationErrorWarning.append(RULES['edgeToolTip'])

            if (float(data['edgeData']['r']) < 0):
                warning = True
                warningList.append("r");
                validationErrorWarning.append({"key":"Status", "data":"Negative r value on Branch.", "custom":"true", "type":"warning"});
                print("Negative r value on Branch - " + data.index + " (" + (data['edgeId']) + ")",
                                         data.edgeData['DOMID'], "edge");

            if (float(data['edgeData']['x']) < 0):
                warning = True
                warningList.append("x")
                validationErrorWarning.append({"key":"Status", "data":"Negative x value on Branch.", "custom":"true", "type":"warning"});
                print ("Negative x value on Branch - " , data['index'] , " (" + (data['edgeId']) , ")",
                                         )
            if (float(data['edgeData']['b']) < 0):
                warning = True
                warningList.append("b");
                validationErrorWarning.append({"key":"Status", "data":"Negative b value on Branch.", "custom":"true", "type":"warning"});
                print ("Negative b value on Branch - " + data.index + " (" + (data['edgeId']) + ")",
                                         data.edgeData['DOMID'], "edge");

            if (float(data['edgeData']['b']) != 0):
                if (abs(math.log10(float(data['edgeData']['x'])) - math.log10(float(data['edgeData']['b']))) >= 1):
                    warning = True
                    warningList.append("charge");
                    validationErrorWarning.append({"key":"charge", "data":"Charge is very different from x.", "custom":"true", "type":"warning"});
                    print (
                        "Branch - " + str(data['index'])
                        + " (" + (data['edgeId']) + ")"
                        + " - charge is very different from x.", "edge");
            if (float(data['edgeData']['rateA']) != 0 ):
                if (float(data['edgeData']['rateA']) > float(data['edgeData']['UB'])):
                    warning = True
                    warningList.append("Rate A");
                    validationErrorWarning.append({"key":"charge", "data":"Rate A thermal limit is larger than the implied upper bound (" + str(
                        data['edgeData']['UB']) + " MVA)", "custom":"true", "type":"warning"});
                    print ("Branch - " , data['index'] , " (" , (
                    data['edgeId']) , ")" , " - Rate A thermal limit is larger than the implied upper bound (", str(
                        data['edgeData']['UB']),
                           'MVA');

            if ((float(
                    data['edgeData']['status']) == 1) and
                    (float(self.nodeDataObjs['dataObjList'][data['source']] ['status'])) == 0 or
                         float(self.nodeDataObjs['dataObjList'][data['target']]['status']) == 0):
                warning = True
                validationErrorWarning.push({"key":"Mismatch Status", "data":"Branch Status 1 where as Bus status is 0.", "custom":"true", "type":"warning"});
                if (float(data.source.status) == 0):
                    print ("Status of Branch - "+ str(data['index']) + " (" +(data['edgeId']) + ")  is 1 where as status for Bus " + data.source.bus_i + " is 0.", data.edgeData.DOMID, "edge");

                else:
                    print ("Status of Branch - "+ str(data['index']) + " (" +(data['edgeId']) + ")  is 1 where as status for Bus " + data.target.bus_i + " is 0.", data.edgeData.DOMID, "edge");

            if (data['edgeType'] == "Transformer"):
                if (float(data['edgeData']['b']) > 0):
                    warning = True
                    warningList.append("charge");
                    validationErrorWarning.append({"key":"Transformer with a non zero charge value.", "data":"Charge value greater than 0 for transformer.", "custom":"true", "type":"warning"});
                    print (
                        "Branch - " + str(data['index'])+ " (" + (data['edgeId']) + ") is a transformer but the charge is greater than zero.",
                         "edge");

            self.edgeDataObjs['dataObjList'][index]["validationWarning"] = validationErrorWarning;
            self.edgeDataObjs['dataObjList'][index]["warning"] = warning;

            ValidationError=[]
            errorList = []
            if (float(data['edgeData']['rateA']) < 0):
                if ((((data['edgeData'].angmax - data['edgeData'].angmin) * Math.PI) / 180) >
                    (2 * Math.max(Math.abs(data.source.Vmax - data.target.Vmin), Math.abs(data.source.Vmin - data.target.Vmax)))):
                    error = True
                    errorList.append("Rate A");
                    validationErrorWarning.append({"key":"Error", "data":"Thermal Limit is Negative.", "custom":"true", "type":"error"});
                    print ("Branch - " + data.index + " (" + (data['edgeId']) + ")" + " - Thermal Limit is Negative.",
                                           data.edgeData['DOMID'], "edge");

            if (float(data['edgeData']['rateB']) < 0):
                if ((((data['edgeData'].angmax - data['edgeData'].angmin) * Math.PI) / 180) >
                (2 * Math.max(Math.abs(data.source.Vmax - data.target.Vmin), Math.abs(data.source.Vmin - data.target.Vmax)))):
                    error = True
                    errorList.append("Rate B");
                    validationErrorWarning.append({"key":"Error", "data":"Rate B (Thermal Limit) is Negative.", "custom":"true", "type":"error"});
                    print (
                        "Branch - " + data.index + " (" + (data['edgeId']) + ")" + " - Rate B (Thermal Limit) is Negative.",
                        data.edgeData['DOMID'], "edge")

            if (float(data['edgeData']['rateC']) < 0):
                errorList =[]
                if ((((data['edgeData'].angmax - data['edgeData'].angmin) * Math.PI) / 180) >
                (2 * math.max(math.abs(data['source']['Vmax'] - data['target']['Vmin']), math.abs(data.source.Vmin - data.target.Vmax)))):
                    error = True
                    errorList.append("Rate C");
                    validationErrorWarning.append({"key":"Error", "data":"Rate C (Thermal Limit) is Negative.", "custom":"true", "type":"error"});
                    print (
                        "Branch - " + data.index + " (" + (data['edgeId']) + ")" + " - Rate C (Thermal Limit) is Negative.",
                        data.edgeData['DOMID'], "edge");

            if (float(data['edgeData']['angmin']) > float(data['edgeData']['angmax'])):
                error = True
                errorList.append("Min angle difference")
                errorList.append("Max angle difference")
                validationErrorWarning.append({"key":"Error", "data":"Infeasible phase angle bounds.", "custom":"true", "type":"error"});
                print  ("Branch - " + data.index + " (" + (data['edgeId']) + ")" + " - Infeasible Voltage Bounds.",
                                       data.edgeData['DOMID'], "edge");





            if ((float(self.nodeDataObjs['dataObjList'][data['source']] ['baseKV'])) !=
                    float(self.nodeDataObjs['dataObjList'][data['target']] ['baseKV']) and
                (data['edgeType'] != "Transformer")):
                error = True
                validationErrorWarning.append({"key":"Error", "data":"Inconsistent KVbase values.", "custom":"true", "type":"error"});
                print ("Branch - " + data['index'] + " (" + (data['edgeId']) + ")" + " - Inconsistent KVbase values.",
                                       data['edgeData']['DOMID'], "edge");

            #float(self.nodeDataObjs['dataObjList'][data['source']]['baseKV'])

            if (float(data['edgeData']['rateA']) != 0):
                if ((float(data['solutionData']["s-s-t"]) > float(data['edgeData']['rateA'])) or (
                float(data['solutionData']["s-t-s"]) > float(data['edgeData']['rateA']))):
                    error = True
                    errorList.append("Apparent power forward");
                    errorList.append("Apparent power reverse");
                    validationErrorWarning.append(
                        {"key": "Error", "data": "Rate A thermal limit violated.", "custom": "true", "type": "error"});
                    print (
                        "Branch - " , data['index'] , " (" , (data['edgeId']) , ")" + " - Rate A thermal limit violated.",
                        );


            if (float(data['solutionData']['angleDiffVal']) < float(data['edgeData']['angmin']) or float(
                data['solutionData']['angleDiffVal']) > float(data['edgeData']['angmax'])):
                error = True
                validationErrorWarning.append(
                    {"key": "Error", "data": "Angle difference is out of bounds.", "custom": "true", "type": "error"});
                print (
                    "Branch - " , data['index'] , " (" , (data['edgeId']) , ")" , " - Angle difference is out of bounds.",
                    )


            self.edgeDataObjs['dataObjList'][index]["validationError"] = validationErrorWarning;
            self.edgeDataObjs['dataObjList'][index]["error"] = error;



            for i in range (0, len(validationErrorWarning)):
                breakstuff = False
                for war in warningList:
                    if(breakstuff == False):
                        if(type(validationErrorWarning[i]) is list ):
                            for j in range(0, len(validationErrorWarning[i])):
                                if (war.find(validationErrorWarning[i][j]['key']) != -1 ):
                                    validationErrorWarning[i][j]["classed"] = "warning"
                                    breakstuff = True
                                    break;

            for i in range (0, len(validationErrorWarning)):
                breakstuff = False
                for err in errorList:
                    if(breakstuff == False):
                        if(type(validationErrorWarning[i]) is list ):
                            for j in range(0, len(validationErrorWarning[i])):
                                if (err.find(validationErrorWarning[i][j]['key']) != -1 ):
                                    validationErrorWarning[i][j]["classed"] = "error"
                                    breakstuff = True
                                    break;





    def validateNode(self):
        nodesData = self.nodeDataObjs['dataObjList']

        for index in range(0, len(nodesData)):
            crtNode = nodesData[index]
            if (len(crtNode['topDecorators']) > 0):
                crtNode = self.validateTopDecorator(crtNode)
            if (len(crtNode['bottomDecorators'] ) > 0):
                crtNode = self.validateBottomDecorator(crtNode)

            erroList = []
            error = False
            if 'validationError' in crtNode:
                validationError = crtNode['validationError']
            else:
                validationError = []
            #validationError = []
            validationError.append(RULES['nodeToolTip'])

            if (float(crtNode['Vmin']) > float(crtNode['Vmax'])):
                error = True
                erroList.push("V Max");
                erroList.push("V Min");
                validationError.append({"key":"Error", "data":"Infeasible Voltage Bounds.", "custom":"true", "type":"error"});
                print("'Bus " + crtNode['bus_i'] + "' - Infeasible Voltage Bounds.", crtNode['bus_i'], "node");

            if ((float(crtNode['Vm']) < float(crtNode['Vmin'])) or (
                float(crtNode['Vm']) > float(crtNode['Vmax']))):
                error = True
                erroList.append("Voltage");
                validationError.append({"key":"Error", "data":"Voltage is out of bounds.", "custom":"true", "type":"error"});
                print("'Bus " + crtNode['bus_i'] + "' - Voltage is out of bounds.", crtNode['bus_i'], "node");


            #for i in range(0, len(validationError)):
            #    if erroList.find(validationError[i]) != -1:
            #        validationError[i]["classed"] = "error"


            for i in range (0, len(validationError)):
                breakstuff = False
                for err in erroList:
                    if(breakstuff == False):
                        if(type(validationError[i]) is list ):
                            for j in range(0, len(validationError[i])):
                                if (err.find(validationError[i][j]['key']) != -1 ):
                                    validationError[i][j]["classed"] = "error"
                                    breakstuff = True
                                    break;


            crtNode["validationError"] = validationError
            crtNode["error"] = error
            nodesData[index] = crtNode

            # TODO return or sth




