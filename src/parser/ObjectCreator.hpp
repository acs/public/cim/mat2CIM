#include <sstream>
#include <string>
#include <fstream>
#include <iostream>
#include "./matpower/MatPowerObject.h"
#include "./matpower/Generator.h"
#include "./matpower/Bus.h"
#include "./matpower/GenCost.hpp"
#include "./matpower/Branch.hpp"

#include "./matpower/mpcObjects.h"
#include <vector>
#include <stdlib.h>

class ObjectCreator {

    float baseMVA = 0;
public:
    // create mpc objects based on the input file
    mpcObjects createObjects(std::vector  <std::vector <std::string > > file){
        // switch to triple list, bus, branch , gen first convert bus, gen, branch
        std::vector <MatPowerObject* > objectList;
        std::vector <GenCost* > genCostList;
        std::vector <Generator* > generatorList;
        std::vector <Bus* > busList;
        std::vector <Branch* > branchList;
        std::string currentBlock = "";



        for(int i = 0; i < file.size(); i ++){

            if (currentBlock.compare("") == 0) {
                if (file[i].size() == 1) {

                    if(file[i][0].compare("baseMVA") == 0) {
                        currentBlock = "baseMVA";
                    }
                    if(file[i][0].compare("BUS") == 0) {
                        // next row is a comment row which explains the format thus we skip this
                        i++;
                        currentBlock = "BUS";
                    }
                    if(file[i][0].compare("GEN") == 0) {
                        // next row is a comment row which explains the format thus we skip this
                        i++;
                        currentBlock = "GEN";
                    }
                    if(file[i][0].compare("BRANCH") == 0) {
                        // next row is a comment row which explains the format thus we skip this
                        i++;
                        currentBlock = "BRANCH";
                    }
                    if(file[i][0].compare("COST") == 0) {
                        // next row is a comment row which explains the format thus we skip this
                        i++;
                        currentBlock = "COST";
                    }
                    continue;
                }

            }

            if(file[i].size() == 1){
                if(file[i][0].compare("END") == 0){ currentBlock = "";}
                //continue;
            }

            if (currentBlock.compare("baseMVA") == 0){
                baseMVA = atoi(file[i][0].c_str());
                Branch::baseMVA = baseMVA;
            }
            if (currentBlock.compare("BUS") == 0){
                    busList.push_back( createBus(file[i]));
                }
            if(currentBlock.compare("GEN") == 0){
                generatorList.push_back(createGenerator(file[i]));
            }
            if (currentBlock.compare("BRANCH") == 0){
                branchList.push_back( createBranch(file[i]));
            }
            if (currentBlock.compare("COST") == 0){
                genCostList.push_back( createGenCost(file[i]));
            }

        }
        // If Gencost exists add them to Generators
        if(genCostList.size() == generatorList.size()){
            for (int i = 0; i < genCostList.size(); i++){
                generatorList[i]->addGenCost(genCostList[i]->startupCost, genCostList[i]->shutdownCost,
                                             genCostList[i]->c2, genCostList[i]->c1,
                                             genCostList[i]->c0);
            }
        }
        // If Generators exists add them to Busses
        if(generatorList.size() > 0){
            for (int i = 0; i < generatorList.size(); i++){
                for (int j = 0; j < busList.size(); ++j) {
                    if (busList[j]->id == generatorList[i]->number){
                        busList[j]->addGenerator(generatorList[i]);
                        generatorList[i]->addBus(busList[j]);
                    }

                }

            }
        }
        /*// Add Generators to ObjectList
         * not needed anymore since we return 3 lists
        for(int i = 0; i < generatorList.size(); i++){
            genCostList.push_back(generatorList[i]);
        }*/
        mpcObjects allMpcObjects = {branchList, busList, generatorList};

        Branch::setBusList(busList);
        return allMpcObjects;
    }

    // create a mpc bus object and return it
    Bus* createBus(std::vector <std::string > line){
        if(line.size() != 13){
            std::cout << "Try to create Bus with invalid num of arguments 13 needed. Got: "
                      << line.size()<<" \n";
            Bus* bus = new Bus();
            return bus;
        }
        Bus* bus = new Bus(atoi(line[0].c_str()), atoi(line[1].c_str()), atof(line[2].c_str()),
                           atof(line[3].c_str()), atof(line[4].c_str()),
                           atof(line[5].c_str()), atoi(line[6].c_str()),
                           atof(line[7].c_str()), atof(line[8].c_str()),
                           atof(line[9].c_str()), atoi(line[10].c_str()),
                           atof(line[11].c_str()), atof(line[12].c_str()));
        return  bus;
    }

    // create a mpc generator object and return it
    Generator* createGenerator(std::vector <std::string > line){
        if(line.size() != 21){
            std::cout << "Try to create Generator with invalid num of arguments 21 needed. Got: "
                      << line.size()<<" \n";
            Generator* generator = new Generator();
            return generator;
        }
       Generator* generator = new Generator(atoi(line[0].c_str()), atof(line[1].c_str()),
                                             atof(line[2].c_str()), atof(line[3].c_str()),
                                             atof(line[4].c_str()), atof(line[5].c_str()),
                                             atoi(line[6].c_str()), atof(line[7].c_str()),
                                             atof(line[8].c_str()), atof(line[9].c_str()),
                                             atof(line[10].c_str()), atof(line[11].c_str()),
                                             atof(line[12].c_str()),atof(line[13].c_str()),
                                             atof(line[14].c_str()), atof(line[15].c_str()),
                                             atof(line[16].c_str()), atof(line[17].c_str()),
                                             atof(line[18].c_str()), atof(line[19].c_str()),
                                             atof(line[20].c_str()));
        return generator;
    }

    // create a mpc branch object and return it
    Branch* createBranch(std::vector <std::string > line){
        if(line.size() != 13){
            std::cout << "Try to create Branch with invalid num of arguments 13 needed. Got: "
                      << line.size()<<" \n";
            Branch* branch = new Branch();
            return branch;
        }
        Branch* branch = new Branch(atoi(line[0].c_str()), atoi(line[1].c_str()), atof(line[2].c_str()),
                           atof(line[3].c_str()), atof(line[4].c_str()),
                           atof(line[5].c_str()), atof(line[6].c_str()),
                           atof(line[7].c_str()), atof(line[8].c_str()),
                           atof(line[9].c_str()), atof(line[10].c_str()),
                           atof(line[11].c_str()),atof(line[11].c_str()));
        return  branch;
    }

    // create a mpc generatorCost object and return it
    GenCost* createGenCost(std::vector <std::string > line){
        if(line.size() != 5){
            std::cout << "Try to create GenCost with invalid num of arguments 5 needed. Got: "
                      << line.size()<<" \n";
            GenCost* genCost = new GenCost();
            return genCost;
        }
        GenCost* genCost = new GenCost(atof(line[0].c_str()), atof(line[1].c_str()), atof(line[2].c_str()),
                                    atof(line[3].c_str()), atof(line[4].c_str()));
        return  genCost;
    }
};