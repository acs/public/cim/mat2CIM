#ifndef MPC2CIM_MATPOWEROBJECT_H
#define MPC2CIM_MATPOWEROBJECT_H
#include "IEC61970.hpp"
#include <vector>

// base MPC Object which provides virtual functions which have to be implemented
class MatPowerObject {

    public:
    // toCim fct should create CIM components which represent the mpc object
    virtual std::vector<BaseClass*> toCim() {
      std::cout<<"...\n";
      std::vector<BaseClass*> a;
      return a;
    }

    // decides if it is a valid mpc Object
    virtual bool isValid() = 0;
    // prints information about the mpc object
    virtual void printAll() = 0;

    // config options used to decide if SVComponents should be created when using toCim
    static std::string useSVforEnergyConsumer;
    static std::string useSVforGeneratingUnit;
    static std::string useSVforExternalNetworkInjection;
    static std::string exportBusTypes;

};
#endif //MPC2CIM_MATPOWEROBJECT_H
