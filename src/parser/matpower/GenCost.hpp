#ifndef MPC2CIM_GENCOST_H
#define MPC2CIM_GENCOST_H
#include <iostream>

class GenCost {

public:
    double startupCost;
    double shutdownCost;
    double c2;
    double c1;
    double c0;

    ~GenCost(){};
    bool isValid() {

        if(this->startupCost < 0){
            std::cout << "startupCost positive double expected. Got: "<< this->startupCost <<"\n";
            return false;
        }
        return true;
    }

    void printAll() {
        std::cout << this->startupCost << " " << this->shutdownCost << " " << this->c2 << " "
                  << this->c1  << " " << this->c0 << "\n";
    }

    GenCost(double startCost, double shutdownCost, double c2, double c1, double c0){
        this->startupCost = startCost;
        this->shutdownCost = shutdownCost;
        this->c2 = c2;
        this->c1 = c1;
        this->c0 = c0;
    }

    GenCost(){
        this->startupCost = -1;

    }
};


#endif //MPC2CIM_GENCOST_H
