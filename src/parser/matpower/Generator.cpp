#include "Generator.h"
#include "Bus.h"

// creates CIM components which represent the mpc generator object
std::vector<BaseClass *> Generator::toCim() {

    std::vector < BaseClass * > retList;

    // create a generating unit
    IEC61970::Base::Generation::Production::GeneratingUnit *generatingUnit = new IEC61970::Base::Generation::Production::GeneratingUnit;
    retList.push_back(generatingUnit);
    // create rotating machine and connect it to the TPNode
    IEC61970::Base::Wires::SynchronousMachine *synchronousMachine = new IEC61970::Base::Wires::SynchronousMachine;
    retList.push_back(synchronousMachine);

    // create Terminal and connect it with synchronousMachine and TPNode
    IEC61970::Base::Core::Terminal *terminal = new IEC61970::Base::Core::Terminal;
    terminal->ConductingEquipment = synchronousMachine;
    terminal->sequenceNumber = 1;
    terminal->connected = true;
    terminal->name =
            "TN" + std::to_string(this->bus->getID()) + "_T" + std::to_string(this->bus->getTerminalCounter());;

    this->bus->getTopologicalNode()->Terminal.push_back(terminal);
    retList.push_back(terminal);

    // name the rotating machine
    synchronousMachine->name = terminal->name + "synchronousMachine";

    // attach synchronous machine to generating unit
    generatingUnit->RotatingMachine.push_back(synchronousMachine);

    // set ratedU of rotating machine to Bus baseKv
    IEC61970::Base::Domain::Voltage *ratedU = new IEC61970::Base::Domain::Voltage;
    ratedU->value = this->bus->getbaseKv();
    synchronousMachine->ratedU = *ratedU;

    // set ratedS of rotating machine to mBase
    IEC61970::Base::Domain::ApparentPower *ratedS = new IEC61970::Base::Domain::ApparentPower;
    ratedS->value = this->mBase;
    synchronousMachine->ratedS = *ratedS;



    if (MatPowerObject::useSVforGeneratingUnit == "BOTH"
        || MatPowerObject::useSVforGeneratingUnit == "FALSE") {

        // set initalP of generating unit to Pg from mpc generator
        IEC61970::Base::Domain::ActivePower *initalP = new IEC61970::Base::Domain::ActivePower;
        initalP->value = this->Pg;
        generatingUnit->initialP = *initalP;

    }
    if(MatPowerObject::useSVforGeneratingUnit == "BOTH"
             || MatPowerObject::useSVforGeneratingUnit == "TRUE" ){
        IEC61970::Base::StateVariables::SvPowerFlow *powerFlow = new IEC61970::Base::StateVariables::SvPowerFlow;
        IEC61970::Base::Domain::ActivePower *pValue = new IEC61970::Base::Domain::ActivePower;
        pValue->value = - 1 * this->Pg;
        powerFlow->p = *pValue;
        IEC61970::Base::Domain::ReactivePower *qValue = new IEC61970::Base::Domain::ReactivePower;
        qValue->value = 0;

        powerFlow->q = *qValue;

        powerFlow->Terminal = terminal;


        retList.push_back(powerFlow);
    }

    if (this->Vg != 0) {
        if (MatPowerObject::useSVforGeneratingUnit == "BOTH" ||
            MatPowerObject::useSVforGeneratingUnit == "FALSE") {
            // create regulating control with target value Vg and attach it to the rotating machine
            IEC61970::Base::Wires::RegulatingControl *regulatingControl = new IEC61970::Base::Wires::RegulatingControl;
            regulatingControl->targetValue = this->Vg * this->bus->getbaseKv();
            synchronousMachine->RegulatingControl = regulatingControl;
            regulatingControl->name = terminal->name + "synchronousMachine" + "regulatingControl";
            retList.push_back(regulatingControl);
        }
        if(MatPowerObject::useSVforGeneratingUnit == "BOTH"
                 || MatPowerObject::useSVforGeneratingUnit == "TRUE" ){
            if(this->bus->type != 3){
                IEC61970::Base::StateVariables::SvVoltage *svVoltage = new IEC61970::Base::StateVariables::SvVoltage;
                svVoltage->v.value = this->Vg * this->bus->getbaseKv();
                svVoltage->TopologicalNode = this->bus->getTopologicalNode();
                retList.push_back(svVoltage);

            }
        }
        // Other Case is handled in Bus
    }
    return retList;
};

void Generator::addBus(Bus *bus) {
    this->bus = bus;
}


Generator::Generator(int number, double Pg, double Qg, double Qmax, double Qmin,
                     double Vg, double mBase, int status, double Pmax, double Pmin,
                     double Pc1, double Pc2, double Qc1min, double Qc1max, double Qc2min,
                     double Qc2max, double rampAgc, double ramp10, double ramp30, double rampQ,
                     double qApf) {

    this->number = number;
    this->Pg = Pg;
    this->Qg = Qg;
    this->Qmax = Qmax;
    this->Qmin = Qmin;
    this->Vg = Vg;
    this->mBase = mBase;
    this->status = status;
    this->Pmax = Pmax;
    this->Pmin = Pmin;
    this->Pc1 = Pc1;
    this->Pc2 = Pc2;
    this->Qc1min = Qc1min;
    this->Qc1max = Qc1max;
    this->Qc2min = Qc2min;
    this->Qc2max = Qc2max;
    this->rampAgc = rampAgc;
    this->ramp10 = ramp10;
    this->ramp30 = ramp30;
    this->rampQ = rampQ;
    this->qApf = qApf;
    this->genCost = new GenCost();
}

void Generator::printAll(){
    std::cout << "working ? ";
    std::cout << "GENERATOR: " << this->number << " " << this->Pg << " " << this->Qmax << " "
              << this->Qmin << " " << this->Vg << " " << this->mBase << " "
              << this->status << " " << this->Pmax << " " << this->Pmin
              << " " << this->Pc1 << " " << this->Pc2 << " " << this->Qc1min
              << " " << this->Qc1max << " " << this->Qc2min << " "
              << this->Qc2max << " " << this->rampAgc << " " << this->ramp10
              << " " << this->ramp30 << " " << this->rampQ << " " << this->qApf;
    this->genCost->printAll();
}

// check if the mpc object is valid or was created with the default constructor
bool Generator::isValid(){

    if (this->number < 1) {
        std::cout << "Generator Number positive integer expected. Got: " << this->number << "\n";
        return false;
    }
    return true;
}

void Generator::addGenCost(double startCost, double shutdownCost, double c2, double c1, double c0) {
    this->genCost = new GenCost(startCost, shutdownCost, c2, c1, c0);
}


