#ifndef MPC2CIM_MPCOBJECTS_H
#define MPC2CIM_MPCOBJECTS_H

// struct which is used to send mpc objects to the MatParser
struct mpcObjects{
    std::vector <Branch* > branchList;
    std::vector <Bus* > busList;
    std::vector <Generator* > generatorList;
};

#endif //MPC2CIM_MPCOBJECTS_H