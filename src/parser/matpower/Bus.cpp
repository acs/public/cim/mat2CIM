#include "Bus.h"
#include "Generator.h"



int Bus::getTerminalCounter() {
    this->terminalCounter++;
    return this->terminalCounter;
}

IEC61970::Base::Topology::TopologicalNode *Bus::getTopologicalNode() {
    if (this->topologicalNode == nullptr) {
        std::cout << "topological node not set for bus " << this->id;
    }
    return this->topologicalNode;
}

void Bus::addGenerator(Generator *generator) {
    this->generator = generator;
}


void Bus::printName() {
    std::cout << "name: " << this->getTopologicalNode()->name << std::endl;
    if (this->getTopologicalNode()->BaseVoltage != nullptr) {
        std::cout << "base voltage: " << this->getTopologicalNode()->BaseVoltage->nominalVoltage.value
                  << std::endl;
    }


    if (this->type == 3) {
        std::list < IEC61970::Base::Core::Terminal * > terminalList = this->getTopologicalNode()->Terminal;

        for (std::list<IEC61970::Base::Core::Terminal *>::iterator it = terminalList.begin();
             it != terminalList.end(); ++it) {
            std::cout << "terminal name: " << (*it)->name << std::endl;
            if (auto networkInjection =
                    dynamic_cast<IEC61970::Base::Wires::ExternalNetworkInjection * >(
                            (*it)->ConductingEquipment)) {
                std::cout << "ENI: " << networkInjection->name << std::endl;


            }
        }
    }

}


IEC61970::Base::Core::Terminal *
    Bus::addTerminal(Bus *bus, IEC61970::Base::Core::ConductingEquipment *consumer, int number) {

        IEC61970::Base::Core::Terminal *terminal = new IEC61970::Base::Core::Terminal;
        terminal->ConductingEquipment = consumer;
        terminal->sequenceNumber = number;
        terminal->connected = true;

        terminal->name = "TN" + std::to_string(bus->id) + "_T" + std::to_string(bus->getTerminalCounter());;
        bus->getTopologicalNode()->Terminal.push_back(terminal);

        //std::std::cout << terminal->name << "\n";
        return terminal;
}


// create a set of cim objects which represents the mpc object and return a list with the mpc objects
std::vector<BaseClass *> Bus::toCim()   {


    // create a TopolologicalNode
    std::vector < BaseClass * > retList;
    IEC61970::Base::Topology::TopologicalNode *topologicalNode = new IEC61970::Base::Topology::TopologicalNode;
    topologicalNode->name = "TN" + std::to_string(this->id);
    this->setTopologicalNode(topologicalNode);
    topologicalNode->mRID = (sole::uuid1()).str();
    retList.push_back(topologicalNode);

    //create a BaseVoltage and connect it to the TPNode
    IEC61970::Base::Core::BaseVoltage *baseVoltage = new IEC61970::Base::Core::BaseVoltage;
    IEC61970::Base::Domain::Voltage *voltage = new IEC61970::Base::Domain::Voltage;
    voltage->value = this->baseKv;
    baseVoltage->nominalVoltage = *voltage;
    topologicalNode->BaseVoltage = baseVoltage;

    retList.push_back(baseVoltage);

    std::vector < BaseClass * > newObjects;
    // create a Load
    if (this->Pd != 0 || this->Qd != 0) {
        newObjects = createLoad();
        retList.insert(retList.end(), newObjects.begin(), newObjects.end());
    }

    // create Shunt
    if (this->Gs != 0 || this->Bs != 0) {
        newObjects = createShunt();
        retList.insert(retList.end(), newObjects.begin(), newObjects.end());
    }
    // create ExternalNetworkInjection
    if (this->type == 3) {
        newObjects = createExternalNetworkInjection();
        retList.insert(retList.end(), newObjects.begin(), newObjects.end());
    }
    if(MatPowerObject::exportBusTypes == "TRUE"){
        newObjects = createBusType();
        retList.insert(retList.end(), newObjects.begin(), newObjects.end());
    }


    return retList;;

};

void Bus::printAll()  {
    std::cout << "BUS: " << this->id << " " << this->type << " " << this->Pd << " "
              << this->Qd << " " << this->Gs << " " << this->Bs << " "
              << this->areaNumber << " " << this->Vm << " " << this->Va
              << " " << this->baseKv << " " << this->zone << " " << this->maxVm
              << " " << this->minVm << "\n";

}

// check if Bus is valid or was created with the default constructor
bool Bus::isValid()  {

    if (this->id < 1) {
        std::cout << "Bus id positive integer expected. Got: " << this->id << std::endl;
        return false;
    }
    if (this->type < 1 || this->type > 4) {
        std::cout << "Bus Types have to be in range(1,4). Got: " << this->type << std::endl;
        return false;
    }
    if (this->Va < -180 || this->Va > 180) {
        std::cout << "Bus Voltage angle in degrees expected (range 0 - 360). Got: " << this->Va << std::endl;
    }
    if (this->areaNumber < 1) {
        std::cout << "Bus Area Number positive integer expected. Got: " << this->areaNumber << std::endl;
    }
    if (this->zone < 1) {
        std::cout << "Bus Zone positive integer expected. Got: " << this->zone << std::endl;
    }
    return true;
}


// create an externalNetworkInjection and all needed components
std::vector<BaseClass *> Bus::createExternalNetworkInjection() {

    std::vector < BaseClass * > retList;
    // create ExternalNetworkInjection
    IEC61970::Base::Wires::ExternalNetworkInjection *networkInjection = new IEC61970::Base::Wires::ExternalNetworkInjection;
    IEC61970::Base::Core::Terminal *terminal = addTerminal(this, networkInjection);
    networkInjection->name = terminal->name + "ExternalNetworkInjection";

    if (MatPowerObject::useSVforExternalNetworkInjection == "FALSE" ||
        MatPowerObject::useSVforExternalNetworkInjection == "BOTH") {

        // create regulating control with target value Vg and attach it to the network injection machine
        IEC61970::Base::Wires::RegulatingControl *regulatingControl = new IEC61970::Base::Wires::RegulatingControl;
        regulatingControl->targetValue = this->Vm;
        if (this->generator != nullptr)
            regulatingControl->targetValue = this->generator->Vg;

        networkInjection->RegulatingControl = regulatingControl;
        regulatingControl->name = terminal->name + "ENI" + "regulatingControl";

        retList.push_back(regulatingControl);

    }
    if (MatPowerObject::useSVforExternalNetworkInjection == "TRUE" ||
            MatPowerObject::useSVforExternalNetworkInjection == "BOTH") {

        IEC61970::Base::StateVariables::SvVoltage *svVoltage = new IEC61970::Base::StateVariables::SvVoltage;
        svVoltage->angle.value = this->Va;
        svVoltage->v.value = this->Vm;

        if (this->generator != nullptr) {
            if (this->generator->Vg != 0)
                svVoltage->v.value = this->generator->Vg * baseKv;

        }

        svVoltage->TopologicalNode = topologicalNode;
        retList.push_back(svVoltage);

    }

    retList.push_back(terminal);
    retList.push_back(networkInjection);
    return retList;
}



std::vector<BaseClass *> Bus::createBusType(){

    std::vector < BaseClass * > retList;

    switch (this->type) {

        case 1 :{
            IEC61970Extension::Simulation::Powerflow::PQBus *pqBus = new IEC61970Extension::Simulation::Powerflow::PQBus;
            pqBus->TopologicalNode = getTopologicalNode();
            retList.push_back(pqBus);
            break;
        }
        case 2 : {
            IEC61970Extension::Simulation::Powerflow::PVBus *pvBus = new IEC61970Extension::Simulation::Powerflow::PVBus;
            pvBus->TopologicalNode = getTopologicalNode();
            retList.push_back(pvBus);
            break;
        }
        case 3 : {
            IEC61970Extension::Simulation::Powerflow::VDBus *vdBus = new IEC61970Extension::Simulation::Powerflow::VDBus;
            vdBus->TopologicalNode = getTopologicalNode();
            retList.push_back(vdBus);
            break;
        }
        case 4 : {
            IEC61970Extension::Simulation::Powerflow::PQBus *pqBus = new IEC61970Extension::Simulation::Powerflow::PQBus;
            pqBus->TopologicalNode = getTopologicalNode();
            retList.push_back(pqBus);
            std::cout << "Warning Type 4 Bus is serialized. "
                    "There exists no equivalent in the CIM standard. Using PQBus instead" << std::endl;
            break;
        }
        default: {
            std::cerr << "unknown type number " << this->type << "\n";
            break;
        }
    }

    return retList;



}

// create a CIM shunt
std::vector<BaseClass *> Bus::createShunt() {

    std::vector < BaseClass * > retList;
    IEC61970::Base::Equivalents::EquivalentShunt *shunt = new IEC61970::Base::Equivalents::EquivalentShunt;
    shunt->name = "Shunt_TN" + std::to_string(this->id);
    IEC61970::Base::Core::Terminal *shuntTerminal = addTerminal(this, shunt);
    IEC61970::Base::Domain::Conductance *conductance = new IEC61970::Base::Domain::Conductance;
    conductance->value = this->Gs / pow(baseKv, 2.0);
    IEC61970::Base::Domain::Susceptance *susceptance = new IEC61970::Base::Domain::Susceptance;
    susceptance->value = this->Bs / pow(baseKv, 2.0);
    shunt->g = *conductance;
    shunt->b = *susceptance;
    retList.push_back(shunt);
    retList.push_back(shuntTerminal);
    return retList;
}

// create a CIM Load
std::vector<BaseClass *> Bus::createLoad() {

    std::vector < BaseClass * > retList;

    // create energyconsumer and svpowerflow with p,q = Pd Qd
    IEC61970::Base::Wires::EnergyConsumer *consumer = new IEC61970::Base::Wires::EnergyConsumer;
    IEC61970::Base::Domain::ActivePower *pValue = new IEC61970::Base::Domain::ActivePower;
    pValue->value = this->Pd;
    IEC61970::Base::Domain::ReactivePower *qValue = new IEC61970::Base::Domain::ReactivePower;
    qValue->value = this->Qd;
    // connect terminal with consumer and add terminal to topologicalNode
    IEC61970::Base::Core::Terminal *terminal = addTerminal(this, consumer);

    consumer->name = terminal->name + "EnergyConsumer";
    if (MatPowerObject::useSVforEnergyConsumer == "FALSE" ||
        MatPowerObject::useSVforEnergyConsumer == "BOTH") {
        consumer->p = *pValue;
        consumer->q = *qValue;

    }
    if (MatPowerObject::useSVforEnergyConsumer == "TRUE" ||
            MatPowerObject::useSVforEnergyConsumer == "BOTH") {
        IEC61970::Base::StateVariables::SvPowerFlow *powerFlow = new IEC61970::Base::StateVariables::SvPowerFlow;

        powerFlow->p = *pValue;
        powerFlow->q = *qValue;

        // connect powerflow with terminal
        powerFlow->Terminal = terminal;
        retList.push_back(powerFlow);
    }

    retList.push_back(terminal);
    retList.push_back(consumer);
    return retList;
}

int Bus::getID(){
    return this->id;
}

double Bus::getbaseKv(){
    return this->baseKv;
}
