#ifndef MPC2CIM_BRANCH_H
#define MPC2CIM_BRANCH_H
#include <math.h>

#include "MatPowerObject.h"


    class Branch : public MatPowerObject {

    /*
   Branch Data Format
        1   f, from bus number
        2   t, to bus number
        3   r, resistance (p.u.)
        4   x, reactance (p.u.)
        5   b, total line charging susceptance (p.u.)
        6   rateA, MVA rating A (long term rating)
        7   rateB, MVA rating B (short term rating)
        8   rateC, MVA rating C (emergency rating)
        9   ratio, transformer off nominal turns ratio ( = 0 for lines )
        (taps at 'from' bus, impedance at 'to' bus,
        i.e. if r = x = 0, then ratio = Vf / Vt)
        10  angle, transformer phase shift angle (degrees), positive => delay
        11  initial branch status, 1 - in service, 0 - out of service
    (2) 12  minimum angle difference, angle(Vf) - angle(Vt) (degrees)
                (2) 13  maximum angle difference, angle(Vf) - angle(Vt) (degrees)
                (The voltage angle difference is taken to be unbounded below
        if ANGMIN < -360 and unbounded above if ANGMAX > 360.
        If both parameters are zero, it is unconstrained.)
        */


    public:


        int f;
        int t;
        double r;
        double x;
        double b;
        double rateA;
        double rateB;
        double rateC;
        double ratio;
        double angle;
        double initialStatus;
        double minAngleDiff;
        double maxAngleDiff;
        static double baseMVA;

        static std::vector<Bus *> BUSLIST;

        static void setBusList(std::vector<Bus *> list) {
            BUSLIST = list;
        };


        static std::vector<Bus *> getBusList(int id) {
            if (BUSLIST.empty()) {
                std::cout << "buslist not initialized when trying to access it" << std::endl;
            }
            return BUSLIST;
        };


        // get the bus with id == id
        Bus *getBusById(int id) {

            if (BUSLIST.empty() == true) {
                std::cout << "buslist not initialized when trying to access it by id" << std::endl;
                return nullptr;
            }
            for (Bus *bus : BUSLIST) {
                if (bus->id == id) {
                    return bus;
                }

            }
            std::cout << "bus with id: " << id << "not found " << std::endl;
            return nullptr;
        }


        /* connect the provided conducting equipment with a new terminal.
         * connect the Terminal with the respective TPNode from the given bus
         */
        IEC61970::Base::Core::Terminal *
        addTerminal(Bus *bus, IEC61970::Base::Core::ConductingEquipment *branch, int number) {

            IEC61970::Base::Core::Terminal *terminal = new IEC61970::Base::Core::Terminal;
            terminal->ConductingEquipment = branch;
            terminal->sequenceNumber = number;
            terminal->connected = true;

            terminal->name = "TN" + std::to_string(bus->id) + "_T" + std::to_string(bus->getTerminalCounter());;
            bus->getTopologicalNode()->Terminal.push_back(terminal);

            //std::std::cout << terminal->name << std::endl ;
            return terminal;
        }


        // Create the CIM Components which can be derived from the mpc Object
        std::vector<BaseClass *> toCim() override {
            // Case PiLine
            if (this->ratio == 0) {

                // create PiLine
                std::vector < BaseClass * > retList;
                IEC61970::Base::Wires::ACLineSegment *piLine = new IEC61970::Base::Wires::ACLineSegment;

                Bus *busF = getBusById(this->f);
                Bus *busT = getBusById(this->t);
                // Connect PiLine with TPNode of BusF and BusT
                IEC61970::Base::Core::Terminal *terminalF = addTerminal(busF, piLine, 1);
                retList.push_back(terminalF);
                retList.push_back(addTerminal(busT, piLine, 2));
                piLine->name = terminalF->name + "PiLine_" + std::to_string(busF->id) + "-" + std::to_string(busT->id);
                //std::cout << piLine->name  << std::endl;

                // Set PiLine values
                IEC61970::Base::Domain::Reactance *reactance = new IEC61970::Base::Domain::Reactance;
                reactance->value = this->x * pow(busF->baseKv, 2.0) / Branch::baseMVA;
                piLine->x = *reactance;
                IEC61970::Base::Domain::Resistance *resistance = new IEC61970::Base::Domain::Resistance;
                resistance->value = this->r * pow(busF->baseKv, 2.0) / this->baseMVA;
                piLine->r = *resistance;
                IEC61970::Base::Domain::Length *length = new IEC61970::Base::Domain::Length;
                length->value = 1;
                piLine->length = *length;
                piLine->bch.value = this->b * Branch::baseMVA / pow(busF->baseKv, 2.0);
                piLine->gch.value = 0;

                // add PiLine to BaseVoltage of TPNode
                busF->getTopologicalNode()->BaseVoltage->ConductingEquipment.push_back(piLine);
                retList.push_back(piLine);
                return retList;

            } else {
                // Case PowerTransformer
                std::vector < BaseClass * > retList;
                // Create PowerTransformer
                IEC61970::Base::Wires::PowerTransformer *powerTrafo =
                        new IEC61970::Base::Wires::PowerTransformer;
                IEC61970::Base::Wires::PowerTransformerEnd *end1 =
                        new IEC61970::Base::Wires::PowerTransformerEnd;
                IEC61970::Base::Wires::PowerTransformerEnd *end2 =
                        new IEC61970::Base::Wires::PowerTransformerEnd;


                Bus *busF = getBusById(this->f);
                Bus *busT = getBusById(this->t);
                // Connect PowerTrafo with terminals at the respective TPNodes
                IEC61970::Base::Core::Terminal *terminalF = addTerminal(busF, powerTrafo, 1);
                IEC61970::Base::Core::Terminal *terminalT = addTerminal(busT, powerTrafo, 2);
                retList.push_back(terminalF);
                retList.push_back(terminalT);

                powerTrafo->name =
                        terminalF->name + "PwerTrafo_" + std::to_string(busF->id) + "-" + std::to_string(busT->id);

                // set vales of the PowerTrafoEnds
                IEC61970::Base::Domain::Resistance *resistance = new IEC61970::Base::Domain::Resistance;
                resistance->value = this->r * pow(busF->baseKv, 2.0) / Branch::baseMVA;
                end1->r = *resistance;

                IEC61970::Base::Domain::Reactance *reactance = new IEC61970::Base::Domain::Reactance;
                reactance->value = this->x * pow(busF->baseKv, 2.0) / Branch::baseMVA;
                end1->x = *reactance;

                IEC61970::Base::Domain::Susceptance *susceptance = new IEC61970::Base::Domain::Susceptance;
                susceptance->value = this->b * Branch::baseMVA / pow(busF->baseKv, 2.0);
                end1->b = *susceptance;

                IEC61970::Base::Domain::Conductance *conductance = new IEC61970::Base::Domain::Conductance;
                conductance->value = 0;
                end1->g = *conductance;

                // Create RatioTapChanger and connect it to PowerTrafoEnd1
                IEC61970::Base::Wires::RatioTapChanger *ratioTapChanger = new IEC61970::Base::Wires::RatioTapChanger;
                ratioTapChanger->neutralStep = 0;
                ratioTapChanger->normalStep = (ratio - 1) * 1000;
                IEC61970::Base::Domain::PerCent *perCent = new IEC61970::Base::Domain::PerCent;
                perCent->value = 0.1;
                ratioTapChanger->stepVoltageIncrement = *perCent;
                end1->RatioTapChanger = ratioTapChanger;
                retList.push_back(ratioTapChanger);

                // set the correct numbers of the PowerTrafoEnds
                end1->endNumber = 1;
                end1->Terminal = terminalF;
                end2->endNumber = 2;
                end2->Terminal = terminalT;


                // set ratedS ratedU and base Voltage of PowerTrafoEnds
                IEC61970::Base::Domain::ApparentPower *ratedS = new IEC61970::Base::Domain::ApparentPower;
                ratedS->value = this->rateA;
                end1->ratedS = *ratedS;
                IEC61970::Base::Domain::Voltage *voltage1 = new IEC61970::Base::Domain::Voltage;
                voltage1->value = busF->baseKv;
                end1->ratedU = *voltage1;

                IEC61970::Base::Domain::Voltage *voltage2 = new IEC61970::Base::Domain::Voltage;
                voltage2->value = busT->baseKv;

                end2->ratedU = *voltage2;
                end2->ratedS = *ratedS;

                IEC61970::Base::Domain::Voltage *voltage = new IEC61970::Base::Domain::Voltage;
                voltage->value = 1;
                IEC61970::Base::Core::BaseVoltage *baseVoltage = new IEC61970::Base::Core::BaseVoltage;
                baseVoltage->nominalVoltage = *voltage;
                end1->BaseVoltage = baseVoltage;


                // push all created components to the return list
                powerTrafo->PowerTransformerEnd.push_back(end1);
                powerTrafo->PowerTransformerEnd.push_back(end2);
                retList.push_back(baseVoltage);
                retList.push_back(powerTrafo);
                retList.push_back(end1);
                retList.push_back(end2);

                return retList;
            }
        };


        // print information about this component
        void printAll() override {
            std::cout << "BRANCH: " << this->f << " " << this->t << " " << this->r << " "
                 << this->x << " " << this->b << " " << this->rateA << " "
                 << this->rateB << " " << this->rateC << " " << this->ratio
                 << " " << this->angle << " " << this->initialStatus << " "
                 << this->minAngleDiff << "" << this->maxAngleDiff << "\n";

        }

        // check if the object was initialized with the default constructor
        bool isValid() override {
            if (this->f < 1) {
               std::cout << "Branch from positive integer expected. Got: " << this->f << "\n";
                return false;
            }
            if (this->ratio < 0) {
               std::cout << "Branch ratio value >=0 expected. Got: " << this->ratio << "\n";
                return false;
            }
            if (this->t < 1) {
               std::cout << "Branch to positive integer expected. Got: " << this->t << "\n";
                return false;
            }
            return true;
        }

        Branch() {
            // Create an Invalid Bus
            this->f = -1;
            this->t = -1;

        }

        Branch(int f, int t, double r, double x,
               double b, double rateA, double rateB, double rateC,
               double ratio, double angle, double initialStatus,
               double minAngleDiff, double maxAngleDiff) {

            this->f = f;
            this->t = t;
            this->r = r;
            this->x = x;
            this->b = b;
            this->rateA = rateA;
            this->rateB = rateB;
            this->rateC = rateC;
            this->ratio = ratio;
            this->angle = angle;
            this->initialStatus = initialStatus;
            this->minAngleDiff = minAngleDiff;
            this->maxAngleDiff = maxAngleDiff;

        }
    };

    std::vector<Bus *> Branch::BUSLIST;
    double Branch::baseMVA = 0.0;

#endif //MPC2CIM_BRANCH_H
