#ifndef MPC2CIM_BUS_H
#define MPC2CIM_BUS_H
#include <iostream>

#include <math.h>

#include "MatPowerObject.h"
#include "Generator.h"
#include "GenCost.hpp"

#include "IEC61970.hpp"
#include "IEC61970Extension.hpp"
#include "../../../libcimpp/thirdparty/sole/sole.hpp"


class Generator;
    class Bus : public MatPowerObject{
        /*
           Bus Data Format
               1   bus number (positive integer)
               2   bus type
                       PQ bus          = 1
                       PV bus          = 2
                       reference bus   = 3
                       isolated bus    = 4
               3   Pd, real power demand (MW)
               4   Qd, reactive power demand (MVAr)
               5   Gs, shunt conductance (MW demanded at V = 1.0 p.u.)
               6   Bs, shunt susceptance (MVAr injected at V = 1.0 p.u.)
               7   area number, (positive integer)
               8   Vm, voltage magnitude (p.u.)
               9   Va, voltage angle (degrees)
               10  baseKV, base voltage (kV)
               11  zone, loss zone (positive integer)
           (+) 12  maxVm, maximum voltage magnitude (p.u.)
           (+) 13  minVm, minimum voltage magnitude (p.u.)
         */


    public:
        int id;
        int type;
        double Pd;
        double Qd;
        double Gs;
        double Bs;
        int areaNumber;
        double Vm;
        double Va;
        double baseKv;
        int zone;
        double maxVm;
        double minVm;
        Generator *generator;
        IEC61970::Base::Topology::TopologicalNode *topologicalNode = nullptr;


        Bus(){// Create an Invalid Bus
            this->id = -1;
            this->type = 0;} ;
        ~Bus(){} ;


        Bus(int id, int type, double Pd, double Qd, double Gs, double Bs,
            int areaNumber, double Vm, double Va, double baseKv, int zone,
            double maxVm, double minVm){
            this->id = id;
            this->type = type;
            this->Pd = Pd;
            this->Qd = Qd;
            this->Gs = Gs;
            this->Bs = Bs;
            this->areaNumber = areaNumber;
            this->Vm = Vm;
            this->Va = Va;
            this->baseKv = baseKv;
            this->zone = zone;
            this->maxVm = maxVm;
            this->minVm = minVm;

        };


        IEC61970::Base::Topology::TopologicalNode *getTopologicalNode() ;
        int getID()  ;
        int getTerminalCounter() ;
        double getbaseKv() ;

        // create a set of cim objects which represents the mpc object and return a list with the mpc objects
        std::vector<BaseClass *> toCim() override;


        void printAll() override ;

        bool isValid() override ;


        void addGenerator(Generator *generator) ;

        void printName() ;



        IEC61970::Base::Core::Terminal *
        addTerminal(Bus *bus, IEC61970::Base::Core::ConductingEquipment *consumer, int number = 1);



        std::vector<BaseClass *> createExternalNetworkInjection();


        std::vector<BaseClass *> createShunt() ;
        std::vector<BaseClass *> createBusType();


        std::vector<BaseClass *> createLoad();



        void setTopologicalNode(IEC61970::Base::Topology::TopologicalNode *node) {
            this->topologicalNode = node;
        }

        int terminalCounter = 0;

    };


#endif //MPC2CIM_BUS_H
