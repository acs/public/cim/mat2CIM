

#ifndef MPC2CIM_GENERATOR_H
#define MPC2CIM_GENERATOR_H
#include <iostream>
#include "MatPowerObject.h"
#include "GenCost.hpp"


class Bus;
class Generator : public MatPowerObject {
    /*
     * Generator Data Format
           1   bus number
           2   Pg, real power output (MW)
           3   Qg, reactive power output (MVAr)
           4   Qmax, maximum reactive power output (MVAr)
           5   Qmin, minimum reactive power output (MVAr)
           6   Vg, voltage magnitude setpoint (p.u.)
           7   mBase, total MVA base of this machine, defaults to baseMVA
           8   status,  >  0 - machine in service
                        <= 0 - machine out of service
           9   Pmax, maximum real power output (MW)
           10  Pmin, minimum real power output (MW)
       (2) 11  Pc1, lower real power output of PQ capability curve (MW)
       (2) 12  Pc2, upper real power output of PQ capability curve (MW)
       (2) 13  Qc1min, minimum reactive power output at Pc1 (MVAr)
       (2) 14  Qc1max, maximum reactive power output at Pc1 (MVAr)
       (2) 15  Qc2min, minimum reactive power output at Pc2 (MVAr)
       (2) 16  Qc2max, maximum reactive power output at Pc2 (MVAr)
       (2) 17  ramp rate for load following/AGC (MW/min)
       (2) 18  ramp rate for 10 minute reserves (MW)
       (2) 19  ramp rate for 30 minute reserves (MW)
       (2) 20  ramp rate for reactive p
       ower (2 sec timescale) (MVAr/min)
       (2) 21  APF, area participation factor
    */
public:
    Generator(){
        this->number = -1;
    };
    ~Generator(){};

    int number;
    double Pg;
    double Qg;
    double Qmax;
    double Qmin;
    double Vg;
    double mBase;
    int status;
    double Pmax;
    double Pmin;
    double Pc1;
    double Pc2;
    double Qc1min;
    double Qc1max;
    double Qc2min;
    double Qc2max;
    double rampAgc;
    double ramp10;
    double ramp30;
    double rampQ;
    double qApf;
    GenCost *genCost;
    Bus *bus;



    Generator(int number, double Pg, double Qg, double Qmax, double Qmin,
              double Vg, double mBase, int status, double Pmax, double Pmin,
              double Pc1, double Pc2, double Qc1min, double Qc1max, double Qc2min,
              double Qc2max, double rampAgc, double ramp10, double ramp30, double rampQ,
              double qApf);

    std::vector<BaseClass *> toCim() override;



    void addBus(Bus *bus);

    void printAll() override;

    bool isValid() override;


    void addGenCost(double startCost, double shutdownCost, double c2, double c1, double c0);


};


#endif //MPC2CIM_GENERATOR_H
