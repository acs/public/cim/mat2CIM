#include <sstream>
#include <string>
#include <fstream>
#include <iostream>
#include "ObjectCreator.hpp"
#include "../ConfigManager.hpp"
#include "CIMModel.hpp"
#include <vector>
#include "IEC61970.hpp"
#include "IEC61970Extension.hpp"
#include "BaseClass.h"
class MatParser{


public:

    /**
     *  get the config from the cfg
     */
    void getConfig(){
        std::cout << "reading config files..." << std::endl;
        this->cfgManager.readConfig();

        MatPowerObject::useSVforGeneratingUnit = this->cfgManager.getUseSVforGeneratingUnit();
        MatPowerObject::useSVforEnergyConsumer = this->cfgManager.getUseSVforEnergyConsumer();
        MatPowerObject::useSVforExternalNetworkInjection = this->cfgManager.getUseSVforExternalNetworkInjection();
        MatPowerObject::exportBusTypes = this->cfgManager.getExportBusTypes();
    }


    /**
     * Read in the file located at input_path
     * Create the respective MPC Objects
     * Create the CIM Objects from the MPC Objects
     */
    std::vector < BaseClass* > parse(std::string input_path){
        ObjectCreator objectCreator;
        // read the inputFile
        std::vector < std::vector < std::string > > file = readFile(input_path);
        //printFile(file);
        // create the mpc objects from the input file
        mpcObjects allMpcObjects = objectCreator.createObjects(file);
        std::vector < BaseClass* > cimObjectList;
        std::vector <BaseClass*> busses;
        std::vector <BaseClass*> branches;
        std::vector <BaseClass*> generators;

        std::cout << std::endl;
        std::cout << allMpcObjects.busList.size() <<" MPC Busses were read in" << std::endl;
        std::cout << allMpcObjects.branchList.size() <<" MPC Branches were read in" << std::endl;
        std::cout << allMpcObjects.generatorList.size() <<" MPC Generators were read in" << std::endl;
        std::cout << std::endl;

        // create the cim objects from the mpc objects
        for(int i = 0; i < allMpcObjects.busList.size(); i++){
            if(allMpcObjects.busList[i]->isValid()){
                //allMpcObjects.busList[i]->printAll();
                busses = allMpcObjects.busList[i]->toCim();
                //allMpcObjects.busList[i]->printName();
                cimObjectList.insert(cimObjectList.end(), busses.begin(), busses.end());
            }
        }
        std::ofstream myfile;
        myfile.open ("mapping_busID_uuid.csv");
        myfile << "# busID, UUID \n";
        for (auto bus : allMpcObjects.busList){
            myfile << bus->id << "," << "\"" << bus->getTopologicalNode()->mRID <<  "\"" << "\n";

        }

        myfile.close();



        for(int i = 0; i < allMpcObjects.branchList.size(); i++){
            if(allMpcObjects.branchList[i]->isValid()){
                //allMpcObjects.branchList[i]->printAll();
                branches = allMpcObjects.branchList[i]->toCim();
                cimObjectList.insert(cimObjectList.end(), branches.begin(), branches.end());
            }
        }

        for(int i = 0; i < allMpcObjects.generatorList.size(); i++){
            if(allMpcObjects.generatorList[i]->isValid()){
                //allMpcObjects.generatorList[i]->printAll();
                generators = allMpcObjects.generatorList[i]->toCim();
                cimObjectList.insert(cimObjectList.end(), generators.begin(), generators.end());
            }
        }


        printCIMObjects(cimObjectList);
        //std::cout << cimObjectList.size() <<"\n";
        return cimObjectList;

    }


    /** print information about which cim objects where created
     * Terminals and SV Components are not printed
     */
    void printCIMObjects( std::vector < BaseClass* > _cimObjectList){

        int tpNode = 0;
        int energyConsumer = 0;
        int baseVoltage = 0;
        int equivalentShunt = 0;
        int externalNetworkInjection = 0;
        int regulatingControl = 0;
        int generatingUnit = 0;
        int synchronousMachine = 0;
        int acLineSegment = 0;
        int ratioTapChanger = 0;
        int powerTrafo = 0;
        for(auto component : _cimObjectList){
            if(dynamic_cast<IEC61970::Base::Topology::TopologicalNode *>(component))
                tpNode ++;
            else if (dynamic_cast<IEC61970::Base::Wires::EnergyConsumer *>(component))
                energyConsumer ++;
            else if (dynamic_cast<IEC61970::Base::Core::BaseVoltage *>(component))
                baseVoltage ++;
            else if (dynamic_cast<IEC61970::Base::Equivalents::EquivalentShunt *>(component))
                equivalentShunt ++;
            else if (dynamic_cast<IEC61970::Base::Wires::ExternalNetworkInjection *>(component))
                externalNetworkInjection++;
            else if (dynamic_cast<IEC61970::Base::Generation::Production::GeneratingUnit *>(component))
                generatingUnit++;
            else if (dynamic_cast<IEC61970::Base::Wires::SynchronousMachine *>(component))
                synchronousMachine ++;
            else if (dynamic_cast<IEC61970::Base::Wires::ACLineSegment *>(component))
                acLineSegment ++;
            else if (dynamic_cast<IEC61970::Base::Wires::RatioTapChanger *>(component))
                ratioTapChanger++;
            else if (dynamic_cast<IEC61970::Base::Wires::PowerTransformer *>(component))
                powerTrafo ++;
            else if (dynamic_cast<IEC61970::Base::Wires::RegulatingControl *>(component))
                regulatingControl++;
        }
        std::cout << std::endl;
        std::cout << tpNode << " Topological Nodes created" << std::endl;
        std::cout << energyConsumer << " Energy Consumer created" << std::endl;
        std::cout << baseVoltage << " BaseVoltages created" << std::endl;
        std::cout << equivalentShunt << " EquivalentShunts created" << std::endl;
        std::cout << externalNetworkInjection << " ExternalNetworkInjections created" << std::endl;
        std::cout << regulatingControl << " Regulating Controls created" << std::endl;
        std::cout << generatingUnit << " Generating Units created" << std::endl;
        std::cout << synchronousMachine << " Synchronous Machines created" << std::endl;
        std::cout << acLineSegment << " ACLineSegments created" << std::endl;
        std::cout << ratioTapChanger << " RatioTapChangers created" << std::endl;
        std::cout << powerTrafo << " Power Trafos created" << std::endl;
        std::cout << std::endl;

    }

    /**
     *  Read in the file located at path and return it as string
     */
    std::vector < std::vector < std::string > > readFile(std::string path){

        const char *input_path = path.c_str();
        std::ifstream infile;
        infile.open(input_path);
        std::string line;
        std::string name;
        std::vector < std::vector < std::string > >file_vector;

        if (infile.is_open()){

            while (std::getline(infile, line))
            {
                std::vector < std::string > line_vector;
                std::istringstream iss(line);

                while ( getline ( iss, line, ','))
                {
                    line_vector.push_back( line );
                }
                file_vector.push_back(line_vector);

            }

        }
        return  file_vector;
    }

    void printFile(std::vector < std::vector < std::string > >  file_vector){

        for (int i = 0; i < file_vector.size(); i++)
        {
            for(int j = 0; j < file_vector[i].size(); j++){

                std::cout << file_vector[i][j];
            }
            std::cout << "\n";
        }
    }
    private:
    ConfigManager cfgManager;
};
