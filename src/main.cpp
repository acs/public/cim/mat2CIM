#include <sys/stat.h>
#include <unistd.h>
#include <string>
#include <fstream>
#include "parser/matpower/Bus.h"
#include "parser/matpower/GenCost.hpp"
#include "parser/matpower/Generator.h"
#include "parser/matpower/Branch.hpp"
#include "parser/MatParser.hpp"
#include <iostream>
#include <getopt.h>
#include "../libcimpp/src/CIMSerializer.hpp"
#include <regex>
#include <stdio.h>





/**
 * Show information to the user how MPC2CIM can be used
 */
void print_argument_help(){
    std::cout << "usage:" << std::endl;
    std::cout << "Parse the files by option -f:" << std::endl;
    std::cout << "./MPC2CIM -f <path/to/file.mat>" << std::endl;
    std::cout << std::endl;
    std::cout << "Change Output File Name by using option -o:" << std::endl;
    std::cout << "./MPC2CIM -o [xmlOutputFileName]" << std::endl;
    std::cout << std::endl;
    std::cout << "By default MPC2CIM creates multiple output files. "
            "If you want all components written in a single output file use the option -s" << std::endl;
    std::cout << std::endl;

    std::cout << "Complete example to parse a file in a directory using a custom output file name" << std::endl;
    std::cout << "./MPC2CIM -f <path/to/file.mat> -o [xmlOutputFileName]" << std::endl;
    exit(1);
}

int main(int argc, char *argv[]) {
    std::string output_file_name;
    output_file_name = "default_output_name";
    static int verbose_flag = 0;
    std::string input_file = "";
    bool multipleFiles = true;
    int c;

    while (1) {
        static struct option long_options[] =
                {
                        /* These options set a flag. */
                        {"verbose", no_argument,       &verbose_flag, 1},
                        {"file",    required_argument, 0,             'f'},
                        {"output",  required_argument, 0,             'o'},
                        {"singleFile",  required_argument, 0,             's'},
                        {0,         0,                 0,             0}
                };
        int option_index = 0;
        c = getopt_long(argc, argv, "o:f:s", long_options, &option_index);
        if (c == -1)
            break;
        switch (c) {
            // Define the name of the output files with -o
            case 'o':
                output_file_name = optarg;
                break;
            // check if the given file is a .mat file
            case 'f':
                optind--;
                for( ;optind < argc && *argv[optind] != '-'; optind++){
                    std::regex mat(".*mat$");
                    if(!regex_match(argv[optind], mat) ){
                        std::cout << "is not a .mat file" << ( argv[optind] ) << std::endl;
                    } else{
                        std::cout << ".mat file is:" << ( argv[optind] ) << std::endl;
                        input_file = argv[optind];
                    }

                }
                break;
            case 's':
                multipleFiles = false;
                break;

            case '?':
                std::cerr << "unknown argument " << c << "\n";
                print_argument_help();
                break;


        }
    }
    // Print unused Arguments
    if (optind < argc)
    {
        printf ("non-option ARGV-elements: ");
        while (optind < argc)
            printf ("%s ", argv[optind++]);
        putchar ('\n');
    }
    // Check if verbose mode is used
    // TODO verbose is not used atm. think about which additonal information should be displayed
    if(verbose_flag)
    {
        std::cout << "verbose activated \n";
    }
    // Parse the input File if it exists
    if (access( input_file.c_str(), F_OK ) != -1 ){
        MatParser matParser;
        matParser.getConfig();
        std::vector < BaseClass* > cimObjectList = matParser.parse(input_file);
        std::cout << cimObjectList.size() << " objects created" <<  std::endl;
        CIMSerializer *serializer = new CIMSerializer;
        serializer->serialize(cimObjectList,output_file_name, multipleFiles);
    } else
        std::cout << "specified file does not exist" << std::endl;


}