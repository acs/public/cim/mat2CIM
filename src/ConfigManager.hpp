
#ifndef MPC2CIM_CONFIGMANAGER_H
#define MPC2CIM_CONFIGMANAGER_H

#include <libconfig.h++>



class ConfigManager {
    public:
        ConfigManager(){
            this->getConfigFile();
        };




        void getConfigFile(){
            try {
                std::string cfgName = "config.cfg";
                if (std::getenv("MPC2CIM_HOME") != NULL){

                    const char* MPC2CIM_HOME = std::getenv("MPC2CIM_HOME");
                    std::cout << "cfg path"<< MPC2CIM_HOME + cfgName << std::endl;
                    cfgName = MPC2CIM_HOME + cfgName;
                    this->cfg.readFile(cfgName.c_str());         ///for release using CMake
                }
                this->cfg.readFile(cfgName.c_str());         ///for release using CMake
            }
            catch (const libconfig::FileIOException &fioex) {
                std::cerr << "I/O error while reading config file." << std::endl;
            }

            catch (const libconfig::ParseException &pex) {
                std::cerr << "Parse error at " << pex.getFile() << ":" << pex.getLine() << " - " << pex.getError() << std::endl;
            }
        }


        void readConfig(){
            std::cout <<"read config file"<< std::endl;
            try {
                this->useSVforEnergyConsumer = this->cfg.lookup("useSVforEnergyConsumer").c_str();

            }
            catch (const libconfig::SettingNotFoundException &nfex) {
                std::cerr << "No useSVforEnergyConsumer setting in configuration file." << std::endl;
            }
            try {
                this->useSVforExternalNetworkInjection = this->cfg.lookup("useSVforExternalNetworkInjection").c_str();

            }
            catch (const libconfig::SettingNotFoundException &nfex) {
                std::cerr << "No useSVforExternalNetworkInjection settings in configuration file." << std::endl;
            }
            try {
                this->useSVforGeneratingUnit= this->cfg.lookup("useSVforGeneratingUnit").c_str();

            }
            catch (const libconfig::SettingNotFoundException &nfex) {
                std::cerr << "No useSVforGeneratingUnit settings in configuration file." << std::endl;
            }
            try {
                this->exportBusTypes= this->cfg.lookup("exportBusTypes").c_str();

            }
            catch (const libconfig::SettingNotFoundException &nfex) {
                std::cerr << "No exportBusTypes setting in configuration file." << std::endl;
            }

        }

        std::string getUseSVforEnergyConsumer(){
            return this->useSVforEnergyConsumer;
        }

        std::string getUseSVforGeneratingUnit(){
            return this->useSVforGeneratingUnit;
        }

        std::string getUseSVforExternalNetworkInjection(){
            return this->useSVforExternalNetworkInjection;
        }

        std::string getExportBusTypes(){
                return this->exportBusTypes;
        }

    private:
        std::string useSVforEnergyConsumer;
        std::string useSVforGeneratingUnit;
        std::string useSVforExternalNetworkInjection;
        std::string exportBusTypes;

        libconfig::Config cfg;

};


#endif //MPC2CIM_CONFIGMANAGER_H
