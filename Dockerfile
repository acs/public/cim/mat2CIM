FROM debian:latest

LABEL \
	org.label-schema.schema-version = "2.1.0" \
	org.label-schema.name = "mpc2cim" \
	org.label-schema.license = "MPL" \
	org.label-schema.vendor = "Institute for Automation of Complex Power Systems, RWTH Aachen University" \
	org.label-schema.author.name = "Jan Dinkelbach" \
	org.label-schema.author.email = "jdinkelbach@eonerc.rwth-aachen.de" \
	org.label-schema.vcs-url = "https://github.com/rwth-acs/mpc2cim"
RUN apt-get update

# Define which libaries should be included in the dockerimg
RUN apt-get install -y git cmake clang build-essential g++ python-dev autotools-dev libicu-dev build-essential libbz2-dev libboost-all-dev libctemplate-dev libconfig++-dev doxygen libxml2 libxml2-dev python-pip octave


RUN pip install scipy

COPY . /MPC2CIM
WORKDIR /MPC2CIM
RUN rm -rf build
RUN mkdir build
WORKDIR build
RUN cmake ..
RUN make -j4
WORKDIR /MPC2CIM
RUN rm -rf src



