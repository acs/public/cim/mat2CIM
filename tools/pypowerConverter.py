#!/usr/bin/env python
import sys
from os.path import basename, splitext, exists
from scipy.io import loadmat
from copy import deepcopy

from numpy import array, zeros, ones, c_


def loadcase(casefile,
        return_as_obj=True, expect_gencost=True, expect_areas=True):

    if return_as_obj == True:
        expect_gencost = False
        expect_areas = False

    info = 0

    # read data into case object
    if isinstance(casefile, basestring):
        # check for explicit extension
        if casefile.endswith(('.py', '.mat')):
            rootname, extension = splitext(casefile)
            fname = basename(rootname)
        else:
            # set extension if not specified explicitly
            rootname = casefile
            if exists(casefile + '.mat'):
                extension = '.mat'
            elif exists(casefile + '.py'):
                extension = '.py'
            else:
                info = 2
            fname = basename(rootname)

        lasterr = ''

        ## attempt to read file
        if info == 0:
            if extension == '.mat':       ## from MAT file
                try:
                    d = loadmat(rootname + extension, struct_as_record=True)
                    if 'ppc' in d or 'mpc' in d:    ## it's a MAT/PYPOWER dict
                        if 'ppc' in d:
                            struct = d['ppc']
                        else:
                            struct = d['mpc']
                        val = struct[0, 0]

                        s = {}
                        for a in val.dtype.names:
                            s[a] = val[a]
                    else:                 ## individual data matrices
                        d['version'] = '1'

                        s = {}
                        for k, v in d.items():
                            s[k] = v

                    s['baseMVA'] = s['baseMVA'][0]  # convert array to float

                except IOError as e:
                    info = 3
                    lasterr = str(e)
            elif extension == '.py':      ## from Python file
                try:
                    #if PY2:
                    #    execfile(rootname + extension)
                    #else:
                    exec(compile(open(rootname + extension).read(),
                                     rootname + extension, 'exec'))

                    try:                      ## assume it returns an object
                        s = eval(fname)()
                    except ValueError as e:
                        info = 4
                        lasterr = str(e)
                    ## if not try individual data matrices
                    if info == 0 and not isinstance(s, dict):
                        s = {}
                        s['version'] = '1'
                        if expect_gencost:
                            try:
                                s['baseMVA'], s['bus'], s['gen'], s['branch'], \
                                s['areas'], s['gencost'] = eval(fname)()
                            except IOError as e:
                                info = 4
                                lasterr = str(e)
                        else:
                            if return_as_obj:
                                try:
                                    s['baseMVA'], s['bus'], s['gen'], \
                                        s['branch'], s['areas'], \
                                        s['gencost'] = eval(fname)()
                                except ValueError as e:
                                    try:
                                        s['baseMVA'], s['bus'], s['gen'], \
                                            s['branch'] = eval(fname)()
                                    except ValueError as e:
                                        info = 4
                                        lasterr = str(e)
                            else:
                                try:
                                    s['baseMVA'], s['bus'], s['gen'], \
                                        s['branch'] = eval(fname)()
                                except ValueError as e:
                                    info = 4
                                    lasterr = str(e)

                except IOError as e:
                    info = 4
                    lasterr = str(e)


                if info == 4 and exists(rootname + '.py'):
                    info = 5
                    err5 = lasterr

    elif isinstance(casefile, dict):
        s = deepcopy(casefile)
    else:
        info = 1

    # check contents of dict
    if info == 0:
        # check for required keys
        if (s['baseMVA'] is None or s['bus'] is None \
            or s['gen'] is None or s['branch'] is None) or \
            (expect_gencost and s['gencost'] is None) or \
            (expect_areas and s['areas'] is None):
            info = 5  ## missing some expected fields
            err5 = 'missing data'
        else:
            ## remove empty areas if not needed
            if hasattr(s, 'areas') and (len(s['areas']) == 0) and (not expect_areas):
                del s['areas']

            ## all fields present, copy to ppc
            ppc = deepcopy(s)
            if not hasattr(ppc, 'version'):  ## hmm, struct with no 'version' field
                if ppc['gen'].shape[1] < 21:    ## version 2 has 21 or 25 cols
                    ppc['version'] = '1'
                else:
                    ppc['version'] = '2'

            if (ppc['version'] == '1'):
                # convert from version 1 to version 2
                ppc['gen'], ppc['branch'] = ppc_1to2(ppc['gen'], ppc['branch']);
                ppc['version'] = '2'

    if info == 0:  # no errors
        if return_as_obj:
            return ppc
        else:
            result = [ppc['baseMVA'], ppc['bus'], ppc['gen'], ppc['branch']]
            if expect_gencost:
                if expect_areas:
                    result.extend([ppc['areas'], ppc['gencost']])
                else:
                    result.extend([ppc['gencost']])
            return result
    else:  # error encountered
        if info == 1:
            sys.stderr.write('Input arg should be a case or a string '
                             'containing a filename\n')
        elif info == 2:
            sys.stderr.write('Specified case not a valid file\n')
        elif info == 3:
            sys.stderr.write('Specified MAT file does not exist\n')
        elif info == 4:
            sys.stderr.write('Specified Python file does not exist\n')
        elif info == 5:
            sys.stderr.write('Syntax error or undefined data '
                             'matrix(ices) in the file\n')
        else:
            sys.stderr.write('Unknown error encountered loading case.\n')

        sys.stderr.write(lasterr + '\n')

        return info

def write_file(casefile):




    if casefile.endswith(('.py', '.mat')):
        rootname, extension = splitext(casefile)
        fname = basename(rootname)
    filename = fname

    data = loadcase(casefile)
    file = open(filename+ '.m', 'w')
    file.write('function mpc = ' + filename + '\n')
    file.write('mpc.version = \'2\';'+ '\n')
    file.write('mpc.baseMVA = ' + str(data['baseMVA']) +';'+ '\n')

    file.write('mpc.bus = [')
    for bus in data['bus']:
        for attr in bus:
            file.write(str(attr) + "    ")
        file.write(';' +'\n')
    file.write(']; \n')
    file.write(' \n')
    file.write(' \n')
    file.write(' \n')


    file.write('mpc.gen = [')
    for gen in data['gen']:
        for attr in gen:
            file.write(str(attr) + "    ")
        file.write(';' +'\n')
    file.write(']; \n')
    file.write(' \n')
    file.write(' \n')
    file.write(' \n')

    file.write('mpc.branch = [')
    for branch in data['branch']:
        for attr in branch:
            file.write(str(attr) + "    ")
        file.write(';' +'\n')

    file.write(']; \n')
    file.write(' \n')
    file.write(' \n')
    file.write(' \n')

    file.write('mpc.gencost = [')
    for gencost in data['gencost']:
        for attr in gencost:
            file.write(str(attr) + "    ")
        file.write(';' +'\n')


    file.write(']; \n')
    file.write(' \n')
    file.write(' \n')
    file.write(' \n')
    file.close()


if len(sys.argv) > 1:
    write_file(sys.argv[1])


