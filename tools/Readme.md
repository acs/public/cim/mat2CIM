# Tools

## PypowerConverter

The pypowerConverter.py Skript allows to convert pypower.py files to .m files which can be converted to .mat files.
The Skript can be executed with the file to convert as argument
```
./tools/pypowerConverter.py LV_SOGNO.py
```